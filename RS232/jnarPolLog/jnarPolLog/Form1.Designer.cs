﻿namespace jnarPolLog
{
  partial class Form1
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if(disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      this.timer1 = new System.Windows.Forms.Timer(this.components);
      this.groupBoxPort = new System.Windows.Forms.GroupBox();
      this.panelActive = new System.Windows.Forms.Panel();
      this.groupBoxTel = new System.Windows.Forms.GroupBox();
      this.labelData = new System.Windows.Forms.Label();
      this.label1 = new System.Windows.Forms.Label();
      this.groupBox1 = new System.Windows.Forms.GroupBox();
      this.labelKNError = new System.Windows.Forms.Label();
      this.label3 = new System.Windows.Forms.Label();
      this.groupBoxFtp = new System.Windows.Forms.GroupBox();
      this.label2 = new System.Windows.Forms.Label();
      this.labelTimeToUpdateFtp = new System.Windows.Forms.Label();
      this.label5 = new System.Windows.Forms.Label();
      this.labelFtpErr = new System.Windows.Forms.Label();
      this.labelFtpOk = new System.Windows.Forms.Label();
      this.button1 = new System.Windows.Forms.Button();
      this.statusStrip1 = new System.Windows.Forms.StatusStrip();
      this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
      this.groupBoxPort.SuspendLayout();
      this.groupBoxTel.SuspendLayout();
      this.groupBox1.SuspendLayout();
      this.groupBoxFtp.SuspendLayout();
      this.statusStrip1.SuspendLayout();
      this.SuspendLayout();
      // 
      // timer1
      // 
      this.timer1.Interval = 500;
      this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
      // 
      // groupBoxPort
      // 
      this.groupBoxPort.Controls.Add(this.panelActive);
      this.groupBoxPort.Location = new System.Drawing.Point(12, 106);
      this.groupBoxPort.Name = "groupBoxPort";
      this.groupBoxPort.Size = new System.Drawing.Size(147, 42);
      this.groupBoxPort.TabIndex = 0;
      this.groupBoxPort.TabStop = false;
      this.groupBoxPort.Text = "COM - xxx";
      // 
      // panelActive
      // 
      this.panelActive.BackColor = System.Drawing.Color.Red;
      this.panelActive.Location = new System.Drawing.Point(19, 18);
      this.panelActive.Name = "panelActive";
      this.panelActive.Size = new System.Drawing.Size(108, 18);
      this.panelActive.TabIndex = 0;
      // 
      // groupBoxTel
      // 
      this.groupBoxTel.Controls.Add(this.labelData);
      this.groupBoxTel.Controls.Add(this.label1);
      this.groupBoxTel.Location = new System.Drawing.Point(12, 12);
      this.groupBoxTel.Name = "groupBoxTel";
      this.groupBoxTel.Size = new System.Drawing.Size(147, 42);
      this.groupBoxTel.TabIndex = 0;
      this.groupBoxTel.TabStop = false;
      this.groupBoxTel.Text = "Telegramy";
      // 
      // labelData
      // 
      this.labelData.AutoSize = true;
      this.labelData.Location = new System.Drawing.Point(57, 18);
      this.labelData.Name = "labelData";
      this.labelData.Size = new System.Drawing.Size(49, 13);
      this.labelData.TabIndex = 0;
      this.labelData.Text = "0000000";
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(16, 18);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(30, 13);
      this.label1.TabIndex = 0;
      this.label1.Text = "Data";
      // 
      // groupBox1
      // 
      this.groupBox1.Controls.Add(this.labelKNError);
      this.groupBox1.Controls.Add(this.label3);
      this.groupBox1.Location = new System.Drawing.Point(164, 106);
      this.groupBox1.Name = "groupBox1";
      this.groupBox1.Size = new System.Drawing.Size(147, 42);
      this.groupBox1.TabIndex = 0;
      this.groupBox1.TabStop = false;
      this.groupBox1.Text = "KNDriver";
      // 
      // labelKNError
      // 
      this.labelKNError.AutoSize = true;
      this.labelKNError.Location = new System.Drawing.Point(57, 18);
      this.labelKNError.Name = "labelKNError";
      this.labelKNError.Size = new System.Drawing.Size(13, 13);
      this.labelKNError.TabIndex = 0;
      this.labelKNError.Text = "0";
      // 
      // label3
      // 
      this.label3.AutoSize = true;
      this.label3.Location = new System.Drawing.Point(16, 18);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(29, 13);
      this.label3.TabIndex = 0;
      this.label3.Text = "Error";
      // 
      // groupBoxFtp
      // 
      this.groupBoxFtp.Controls.Add(this.label2);
      this.groupBoxFtp.Controls.Add(this.labelTimeToUpdateFtp);
      this.groupBoxFtp.Controls.Add(this.label5);
      this.groupBoxFtp.Controls.Add(this.labelFtpErr);
      this.groupBoxFtp.Controls.Add(this.labelFtpOk);
      this.groupBoxFtp.Location = new System.Drawing.Point(12, 60);
      this.groupBoxFtp.Name = "groupBoxFtp";
      this.groupBoxFtp.Size = new System.Drawing.Size(299, 42);
      this.groupBoxFtp.TabIndex = 0;
      this.groupBoxFtp.TabStop = false;
      this.groupBoxFtp.Text = "Ftp";
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Location = new System.Drawing.Point(191, 18);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(70, 13);
      this.label2.TabIndex = 2;
      this.label2.Text = "Další upload:";
      // 
      // labelTimeToUpdateFtp
      // 
      this.labelTimeToUpdateFtp.AutoSize = true;
      this.labelTimeToUpdateFtp.Location = new System.Drawing.Point(259, 18);
      this.labelTimeToUpdateFtp.Name = "labelTimeToUpdateFtp";
      this.labelTimeToUpdateFtp.Size = new System.Drawing.Size(34, 13);
      this.labelTimeToUpdateFtp.TabIndex = 1;
      this.labelTimeToUpdateFtp.Text = "00:00";
      // 
      // label5
      // 
      this.label5.AutoSize = true;
      this.label5.Location = new System.Drawing.Point(66, 18);
      this.label5.Name = "label5";
      this.label5.Size = new System.Drawing.Size(12, 13);
      this.label5.TabIndex = 0;
      this.label5.Text = "/";
      // 
      // labelFtpErr
      // 
      this.labelFtpErr.AutoSize = true;
      this.labelFtpErr.Location = new System.Drawing.Point(78, 18);
      this.labelFtpErr.Name = "labelFtpErr";
      this.labelFtpErr.Size = new System.Drawing.Size(49, 13);
      this.labelFtpErr.TabIndex = 0;
      this.labelFtpErr.Text = "0000000";
      // 
      // labelFtpOk
      // 
      this.labelFtpOk.Location = new System.Drawing.Point(16, 18);
      this.labelFtpOk.Name = "labelFtpOk";
      this.labelFtpOk.Size = new System.Drawing.Size(49, 13);
      this.labelFtpOk.TabIndex = 0;
      this.labelFtpOk.Text = "0000000";
      this.labelFtpOk.TextAlign = System.Drawing.ContentAlignment.TopRight;
      // 
      // button1
      // 
      this.button1.Location = new System.Drawing.Point(206, 25);
      this.button1.Name = "button1";
      this.button1.Size = new System.Drawing.Size(75, 23);
      this.button1.TabIndex = 1;
      this.button1.Text = "Test";
      this.button1.UseVisualStyleBackColor = true;
      this.button1.Visible = false;
      this.button1.Click += new System.EventHandler(this.button1_Click);
      // 
      // statusStrip1
      // 
      this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
      this.statusStrip1.Location = new System.Drawing.Point(0, 158);
      this.statusStrip1.Name = "statusStrip1";
      this.statusStrip1.Size = new System.Drawing.Size(327, 22);
      this.statusStrip1.TabIndex = 2;
      this.statusStrip1.Text = "statusStrip1";
      // 
      // toolStripStatusLabel1
      // 
      this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
      this.toolStripStatusLabel1.Size = new System.Drawing.Size(118, 17);
      this.toolStripStatusLabel1.Text = "toolStripStatusLabel1";
      // 
      // Form1
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(327, 180);
      this.Controls.Add(this.statusStrip1);
      this.Controls.Add(this.button1);
      this.Controls.Add(this.groupBox1);
      this.Controls.Add(this.groupBoxFtp);
      this.Controls.Add(this.groupBoxTel);
      this.Controls.Add(this.groupBoxPort);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
      this.MaximizeBox = false;
      this.Name = "Form1";
      this.Text = "JPL 1.02";
      this.Load += new System.EventHandler(this.Form1_Load);
      this.groupBoxPort.ResumeLayout(false);
      this.groupBoxTel.ResumeLayout(false);
      this.groupBoxTel.PerformLayout();
      this.groupBox1.ResumeLayout(false);
      this.groupBox1.PerformLayout();
      this.groupBoxFtp.ResumeLayout(false);
      this.groupBoxFtp.PerformLayout();
      this.statusStrip1.ResumeLayout(false);
      this.statusStrip1.PerformLayout();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.Timer timer1;
    private System.Windows.Forms.GroupBox groupBoxPort;
    private System.Windows.Forms.Panel panelActive;
    private System.Windows.Forms.GroupBox groupBoxTel;
    private System.Windows.Forms.Label labelData;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.GroupBox groupBox1;
    private System.Windows.Forms.Label labelKNError;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.GroupBox groupBoxFtp;
    private System.Windows.Forms.Label label5;
    private System.Windows.Forms.Label labelFtpErr;
    private System.Windows.Forms.Label labelFtpOk;
    private System.Windows.Forms.Button button1;
    private System.Windows.Forms.StatusStrip statusStrip1;
    private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
    private System.Windows.Forms.Label labelTimeToUpdateFtp;
    private System.Windows.Forms.Label label2;
  }
}

