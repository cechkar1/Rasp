﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO.Ports;

namespace jnarPolLog
{
  class SerialDataKn
  {
    private SerialPort port;
    private ByteData dataRx, header;
    private ByteData logRawData;  //data co se logujou - prijima se vse pro log
    private int stateRx = 0;
    private UInt16 sizeDataToRx = 0;
    private LogToFile log;
    private int errorRx = 0;
    private string errorString = "";
    private ByteDataQueue dataRxQueue;


    public SerialDataKn(string _port, int _baudRate, LogFileParams _logParams)
    {
      log = new LogToFile(_logParams);
      dataRx = new ByteData(1500); //TODO parametr konstruktoru
      header = new ByteData(1500);
      logRawData = new ByteData(1500);
      dataRxQueue = new ByteDataQueue(10, 1500);
      header.SetByte(0, 0xFF);
      header.SetByte(1, 0xFF);
      header.SetByte(2, 0xC4);
      header.SetByte(3, 0xD7);
      port = new SerialPort(_port, _baudRate, Parity.None, 8, StopBits.One);
      port.DataReceived += new SerialDataReceivedEventHandler(port_DataReceived);
      port.Open();
    }

    //do error string prida zaznam
    private void setErrorString(string _err)
    {
      if(errorString != "")
      {
        //uz tam neco je, pridat
        if(errorString.Length < 1000)
        {
          //jeste rozumna delka
          errorString += ", " + _err;
        }
        else
        {
          if(errorString.Substring(errorString.Length - 1) != "W")
          {
            errorString += ", " + _err + ", OVERFLOW";
          }
        }

      }
      else
      {
        errorString = _err;
      }
    }
    
    public void SendData(ref ByteData _data)
    {
      //log.LogDataHHMMSS_SSS("dataTx: ", ref _data);
      header.CopyToOffsDataFrom(ref _data, 4); //slozeni dat do jednoho bloku
      log.LogDataHHMMSS_SSS("bytesTx: ", ref header);
      port.Write(header.GetData(), 0, header.GetSizeData());
    }

    public bool isRcvData()
    {
      bool result = false;

      if(dataRxQueue.IsEmpty() == false)
      {
        result = true;
      }

      return result;
    }

    public void GetRcvData(ByteData _data)
    {
      if(dataRxQueue.IsEmpty() == false)
      {
        dataRxQueue.GetData(ref _data);
      }
    }
    
    //povoli/zakaze logovani
    public void EnableLogSerial(bool _enable)
    {
      log.EnableLog(_enable);
    }

    //smaze log
    public void ClearLog()
    {
      log.ClearLog();
    }

    //vytahne pocet chyb
    public int GetRxErr()
    {
      return errorRx;
    }

    //vytahne error string
    public string GetErrorString()
    {
      return errorString;
    }

    //smaze error string
    public void ClearErrorString()
    {
      errorString = "";
    }

    private void port_DataReceived(object sender, SerialDataReceivedEventArgs e)
    {
      int tempRx = 0;

      while(port.BytesToRead > 0)
      {
        tempRx = port.ReadByte();

        //logovat vzdy
        if(logRawData.GetSizeData() < logRawData.GetSizeMem())
        {
          logRawData.AddByte(tempRx);
        }
        else
        {
          //zapsat do souboru a zacit znova
          log.LogDataYYYYMMDD_HHMMSS("bytesRX: ", ref logRawData); //zaloguje se co prislo
          logRawData.Clear();
          logRawData.AddByte(tempRx);
        }

        switch(stateRx)
        {
          case 0:
            //zacatek prijmu, testnem jestli prislo 0xFF
            if(tempRx == 0xFF)
            {
              //nastavi se stav prijmu 0xff a cekani na 0xc4
              stateRx = 1;
            }
            break;

          case 1:
            //zacatek prijmu, tesnem jestli prislo 0xc4
            if(tempRx == 0xC4)
            {
              //nastavi se stav cekani na 0xd7
              stateRx = 2;
            }
            else if(tempRx == 0xFF)
            {
              stateRx = 1; //stav zustava
            }
            else
            {
              stateRx = 0; //znovu na zacatek
              log.LogDataYYYYMMDD_HHMMSS("bytesRX: ", ref logRawData); //zaloguje se co prislo
              logRawData.Clear();
            }
            break;

          case 2:
            //zacatek prijmu, tesnem jestli prislo 0xd7
            if(tempRx == 0xD7)
            {
              //zacatek prijmu dat
              stateRx = 3;
              dataRx.TrimSize(0);
            }
            else
            {
              stateRx = 0; //znovu na zacatek
              log.LogDataYYYYMMDD_HHMMSS("bytesRX: ", ref logRawData); //zaloguje se co prislo
              logRawData.Clear();
            }
            break;

          case 3:
            //zacatek prijmu dat
            if(dataRx.GetSizeData() < 5)
            {
              dataRx.AddByte(tempRx);
            }

            if(dataRx.GetSizeData() == 5)
            {
              sizeDataToRx = dataRx.GetUi16(3, true);

              if(sizeDataToRx > dataRx.GetSizeMem())
              {
                stateRx = 0; //znovu na zacatek - neni kam ulozit
                dataRx.TrimSize(0);
                errorRx++;
                setErrorString("enought space for rx " + logRawData.ToHexStrAll());
                log.LogDataYYYYMMDD_HHMMSS("bytesRX: ", ref logRawData); //zaloguje se co prislo
                logRawData.Clear();
              }
              else
              {
                stateRx = 4;
              }
            }
            break;

          case 4:
            dataRx.AddByte(tempRx);
            if(dataRx.GetSizeData() == sizeDataToRx + 7) //5B hlavicka + 2 crc
            {
              sizeDataToRx = 0;
              stateRx = 0;     
              log.LogDataHHMMSS_SSS("bytesRx: ", ref logRawData);
              
              //kdyz je data kam ulozit, tak ulozit, jinak chyba
              if(dataRxQueue.IsFull() == false)
              {
                dataRxQueue.AddData(ref dataRx);
              }
              else
              {
                setErrorString("rx fifo overfl, rawdataRx: " + logRawData.ToHexStrAll());
                errorRx++;
              }

              dataRx.Clear(); //pripravir dalsi prijem
              logRawData.Clear(); //a raw data se mohou smaznout
            }
            break;

          default:
            break;

        }
      }

    }

  }
}
