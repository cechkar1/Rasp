/* 
 * File:   raspFTP.c.c
 * Author: karel
 *
 * Created on September 2, 2015, 5:21 PM
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <linux/serial.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <termios.h>
#include "terminal.h"
#include "mainHeather.h"

#define PORT "/dev/ttyUSB0"
/*
 * 
 */
int main(int argc, char** argv) {

        unsigned int message_size = 1;
        message_size = message_size << 8;
        message_size |= 1;
        printf("ahoj %d\n",message_size);
	int fd = open (PORT, O_RDWR | O_NOCTTY | O_SYNC);
	if (fd < 0) {
            printf("Couldn't open the port (%s).\n",PORT);
            return errno;
	}
	serial_set_attributes(fd, B19200, CS8, 1, 0, 0);
        char *buff=(char*)malloc(MAX_LOG_LENGTH*sizeof(char));
        memset(buff,0,sizeof(buff));
        memmove(buff,buff,100);
        int bytes_read=0;
        int tele_type=0;
        int check=0;
        while (1){
            printf("pruchod %d\n",check);
            tele_type=header_check(fd,buff,&bytes_read);
            if(tele_type==0x0A) REQ(fd,buff[4],buff,&bytes_read);
            else if (tele_type==0x0B)PASS(fd,buff[4],buff,&bytes_read);
            else {
                printf("Command number - 0x%x\n",tele_type);
                memmove(buff,buff+7,(bytes_read-7)*sizeof(*buff));
                bytes_read-=7;
            }
            check++;
        }
        free(buff);
        close(fd);  
    
    
//    char command[MAX_COMMAND_LENGTH]={0};
//    
//    strcat(command,"curl -T ");
//    strcat(command,FILE);
//    strcat(command," ");
//    strcat(command,FTP_ADD);
//    strcat(command," -u ");
//    strcat(command,USR_PWD);
//
//    system(command);
//    return (EXIT_SUCCESS);
}

