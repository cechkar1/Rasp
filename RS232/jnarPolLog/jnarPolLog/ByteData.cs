﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jnarPolLog
{
  class ByteData
  {
    private UInt16 sizeMem = 0;
    private UInt16 sizeData = 0;
    byte[] buffer;

    public ByteData(UInt16 _sizeMem)
    {
      buffer = new byte[_sizeMem];
      sizeMem = _sizeMem;
    }

    public ByteData(byte[] _buffer)
    {
      buffer = _buffer;
      sizeMem = Convert.ToUInt16(_buffer.Length);
      sizeData = Convert.ToUInt16(_buffer.Length);
    }

    public byte[] GetData()
    {
      return buffer;
    }

    public UInt16 GetSizeData()
    {
      return sizeData;
    }

    public UInt16 GetSizeMem()
    {
      return sizeMem;
    }

    public byte GetByte(UInt16 _offset)
    {
      return buffer[_offset];
    }

    public UInt16 GetUi8(UInt16 _offset)
    {
      return buffer[_offset];
    }

    //vrati 16bit cislo, horni byte ulozen na nizsim offsetu
    public UInt16 GetUi16(UInt16 _offset, bool isLittleEndian)
    {
      UInt16 tempH, tempL, tempRet;

      if(isLittleEndian == true)
      {
        tempH = buffer[_offset];
        tempL = buffer[_offset + 1];
      }
      else
      {
        tempL = buffer[_offset];
        tempH = buffer[_offset + 1];
      }

      tempH = System.Convert.ToUInt16(tempH * 256);  //soupnem o 8B doleva
      tempRet = System.Convert.ToUInt16(tempH + tempL);

      return tempRet;
    }

    public void SetByte(UInt16 _offset, byte _data)
    {
      if((_offset + 1) > sizeData)
      {
        sizeData = System.Convert.ToUInt16(_offset + 1u);
      }

      buffer[_offset] = _data;
    }

    //nastavi horni byte na nizsi offset
    public void SetUi16(UInt16 _offset, UInt16 _data, bool isLittleEndian)
    {
      byte[] temp = System.BitConverter.GetBytes(_data);

      if((_offset + 2) > sizeData)
      {
        sizeData = System.Convert.ToUInt16(_offset + 2u);
      }

      if(isLittleEndian == true)
      {
        buffer[_offset] = temp[1];
        buffer[_offset + 1] = temp[0];
      }
      else
      {
        buffer[_offset] = temp[0];
        buffer[_offset + 1] = temp[1];
      }
    }

    //prida byte za posledni ulozeny byte
    public void AddByte(int _data)
    {
      byte[] temp = System.BitConverter.GetBytes(_data);
      buffer[sizeData] = temp[0];
      sizeData++;
    }

    //zmensi data na pozadovanou velikost
    public void TrimSize(UInt16 _trim)
    {
      sizeData = _trim;
    }

    //zapise do celeho bufferu nuly
    public void Clear()
    {
      UInt16 x = 0;
      for(x = 0;x < sizeMem;x++)
      {
        buffer[x] = 0;
      }
      sizeData = 0;
    }

    //zkopiruje data z jineho ByteData
    public void CopyAllFrom(ref ByteData _data)
    {
      UInt16 x = 0;

      for(x = 0;x < _data.GetSizeData();x++)
      {
        buffer[x] = _data.GetByte(x);
      }
      sizeData = _data.GetSizeData();
    }

    //zkopiruje kus dat z jineho ByteData od zadaneho offsetu a zvolene delky
    public void CopySubDataFrom(ref ByteData _data, UInt16 _offset, UInt16 _size)
    {
      UInt16 x = 0;

      for(x = 0;x < _size;x++)
      {
        buffer[x] = _data.GetByte(Convert.ToUInt16(x + _offset));
      }
      sizeData = _size;
    }

    //zkopiruje kus dat z jineho ByteData od zadaneho offsetu a zvolene delky
    public void CopySubDataFrom(ref byte[] _data, UInt16 _offset, UInt16 _size)
    {
      UInt16 x = 0;

      for(x = 0;x < _size;x++)
      {
        buffer[x] = _data[x + _offset];
      }
      sizeData = _size;
    }

    //zkopiruje kus dat z jineho ByteData na zadany offset od zadaneho offsetu a zvolene delky
    public void CopyToOffsSubDataFrom(ref ByteData _data, UInt16 _srcOffset, UInt16 _size, UInt16 _dstOffs)
    {
      UInt16 x = 0;

      for(x = 0;x < _size;x++)
      {
        buffer[x + _dstOffs] = _data.GetByte(Convert.ToUInt16(x + _srcOffset));
      }
      sizeData = Convert.ToUInt16(_size + _dstOffs);
    }

    //zkopiruje kus dat z jineho byte[] na zadany offset od zadaneho offsetu a zvolene delky
    public void CopyToOffsSubDataFrom(ref byte[] _data, UInt16 _srcOffset, UInt16 _size, UInt16 _dstOffs)
    {
      UInt16 x = 0;

      for(x = 0;x < _size;x++)
      {
        buffer[x + _dstOffs] = _data[x + _srcOffset];
      }
      sizeData = Convert.ToUInt16(_size + _dstOffs);
    }

    //zkopiruje vse ze zadanych dat, kopirovana data uklada od zadaneho offsetu
    public void CopyToOffsDataFrom(ref ByteData _data,UInt16 _dstOffset)
    {
      UInt16 x = 0;

      for(x = 0;x < _data.GetSizeData();x++)
      {
        buffer[x + _dstOffset] = _data.GetByte(Convert.ToUInt16(x));
      }

      sizeData = Convert.ToUInt16(_data.GetSizeData() + _dstOffset);
    }

    //zkopiruje vse ze zadanych dat, kopirovana data uklada od zadaneho offsetu
    public void CopyToOffsDataFrom(ref byte[] _data, UInt16 _dstOffset)
    {
      UInt16 x = 0;

      for(x = 0;x < _data.Length;x++)
      {
        buffer[x + _dstOffset] = _data[x];
      }

      sizeData = Convert.ToUInt16(_data.Length + _dstOffset);
    }

    public string ToHexStrAll()
    {
      string result = "";

      try
      {
        result = BitConverter.ToString(buffer, 0, sizeData);
      }
      catch
      {
        result = "to hexsting convert error";
      }

      return result;
    }

    public string ToHexStrOffs(int _start, int _size)
    {
      string result = "";

      try
      {
        result = BitConverter.ToString(buffer, _start, _size);
      }
      catch
      {
        result = "to hexsting convert error";
      }

      return result;
    }

  }
}
