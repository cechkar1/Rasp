﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace jnarPolLog
{
  class SimpleLogToFile
  {
    private TextWriter logFile;
    private bool open;


    public SimpleLogToFile()
    {
    }

    //otevre soubor - true doplni, false vytvori novy/smaze obsah
    public bool OpenFile(string _name, bool _append)
    {
      try
      {
        logFile = new StreamWriter(_name, _append);
        open = true;
      }
      catch
      {
        try
        {
          logFile.Close();
        }
        catch
        {
        }

        open = false;
      }

      return open;
    }

    public void Close()
    {
      try
      {
        logFile.Close();
        open = false;
      }
      catch
      {
      }
    }


    public void LogDataYYYYMMDD_HHMMSS(string _text, ref ByteData _data)
    {
      if(open == true)
      {
        try
        {
          logFile.Write("{0:d4}-{1:d2}-{2:d2} {3:d2}:{4:d2}:{5:d2}{6}", 
                         DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day,
                         DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second, _text);

          string temp = _data.ToHexStrAll();
          logFile.Write(temp);

          logFile.Write("\n");
        }
        catch
        {
        }
      }
    }




  }
}