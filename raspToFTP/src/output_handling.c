#include "mainHeather.h"
#include "terminal.h"








void clear_line(){
    int i;
    for(i=0;i<80;i++){
    	printf(" ");
    }
    printf("\r");
}
void write_to_log_file(char * text){
    clear_line();
    char * time_stamp=get_time_for_output_log(GPS_TIME);
    printf("%s%s",time_stamp,text);
    fflush(stdout);
    if(g_err_log!=NULL){
        FILE * err_fp=fopen(g_err_log,"a");
        if (err_fp==NULL){
            printf("Log file couldn't be opened.\n");
	    free(time_stamp);
            return;
        }
        fprintf(err_fp,"%s%s",time_stamp,text);
        fclose(err_fp);
    }
    free(time_stamp);
}

char * create_output_name(char* name_end){
    	char * new_fileName=(char*)malloc(250*sizeof(char));
        //add a directory in front of the name
	sprintf(new_fileName,"%s/",config->logFold);
	int i=strlen(config->logFold);
        pthread_mutex_lock(&mutex_lock);
            new_fileName[i+1]=global_time[10];
            new_fileName[i+1+1]=global_time[11];
            new_fileName[i+2+1]=global_time[8];
            new_fileName[i+3+1]=global_time[9];
            new_fileName[i+4+1]=global_time[6];
            new_fileName[i+5+1]=global_time[7];
            new_fileName[i+6+1]='_';
            new_fileName[i+7+1]=global_time[0];
            new_fileName[i+8+1]=global_time[1];
            new_fileName[i+9+1]=global_time[2];
            new_fileName[i+10+1]=global_time[3];
            new_fileName[i+11+1]=global_time[4];
            new_fileName[i+12+1]=global_time[5];
        pthread_mutex_unlock(&mutex_lock);
	new_fileName[i+13+1]='\0';
	strcat(new_fileName,"UTC");
	strcat(new_fileName,name_end);
	return new_fileName;
}

char * get_time_for_output_log(){
	char* log_time=(char*)malloc(50*sizeof(char));
        log_time[0]='2';
        log_time[1]='0';
        pthread_mutex_lock(&mutex_lock);
        //need to switch day and year for the correct format
            log_time[2]=global_time[10];
            log_time[3]=global_time[11];
            log_time[4]='-';
            log_time[5]=global_time[8];
            log_time[6]=global_time[9];
            log_time[7]='-';
            log_time[8]=global_time[6];
            log_time[9]=global_time[7];
            log_time[10]=' ';
            log_time[11]=global_time[0];
            log_time[12]=global_time[1]; //in UTC now
            log_time[13]=':';
            log_time[14]=global_time[2];
            log_time[15]=global_time[3];
            log_time[16]=':';
            log_time[17]=global_time[4];
            log_time[18]=global_time[5];
        pthread_mutex_unlock(&mutex_lock);
    	log_time[19]=';';
    	log_time[20]=' ';
	log_time[21]='\0';
	return log_time;
}

void *thread_Func_system(void *upload_name){
    //avoiding signals to complete the upload
    struct sigaction sigact;
    sigemptyset(&sigact.sa_mask);
    sigact.sa_flags   = SA_SIGINFO;
    sigact.sa_handler = sighandler;
    sigaction(SIGINT, &sigact, 0);
    sigaction(SIGTERM,&sigact,0);
    sigaction(SIGQUIT,&sigact,0);
    
    char command[MAX_COMMAND_LENGTH]={0};
    sprintf(command,"curl -s -T %s ftp://%s -u %s:%s > /dev/null 2>&1",(char*)upload_name,config->FTP_IP,config->username,config->password); //> /dev/null 2>&1

    //wait for potential thread  write_to_output_file to write last data
    sleep(WAIT_TIME+1);
    int curl_return=system(command);
    char log[200];
    if(curl_return){
        sprintf(log,"File %s wasn't uploaded. Curl Error %d occured.\n",(char*)upload_name,curl_return);
        write_to_log_file(log);
    }
    else {
        sprintf(log,"File %s was successfully uploaded.\n",(char*)upload_name);
        write_to_log_file(log);
    }
    free(upload_name);
    return NULL;
}

int upload_file(char * upload_name,int detached){
    char *up_name=(char*)malloc(400*sizeof(char));
    strcpy(up_name,upload_name);
    //need to do a separate thread for this system call, because uploading could delay, so now the program isn't waiting for this call;
    pthread_t pth;
    pthread_create(&pth, NULL,thread_Func_system, (void *)up_name);
    if (detached) pthread_detach(pth);
    else pthread_join(pth,NULL);
    return 0;
}



int write_bytes(int output_fd,unsigned char * message,int amount){
    int i=0;
    int check=0;
    char byte[3]; //3 because of \0
    for(;i<amount;i++){
        sprintf(byte,"%02x",message[i] & 0xFF); //we have to format the bytes, because log is in ASCII format
        check=write(output_fd,byte,2);
	printf("%s",byte);
        if (check!=2)
            return 1;
        if(i!=amount-1) {
		check=write(output_fd,"-",1);
		printf("-");
	}
        else{ 
            check=write(output_fd,"\n",1);
	    printf("\n");
	}
        if (check!=1)
            return 1;
    }
    return 0;
}

void * write_to_output(void* arguments){
    T_args * args = (T_args*)arguments;
    int output_fd=open(args->output_name, O_APPEND | O_RDWR | O_CREAT,0755);
    int i=0;
    char log[200];
    while(output_fd<0){
        sprintf(log,"Couldn't open the file %s\n",args->output_name);
        write_to_log_file(log);
        sleep(1);
        output_fd=open(args->output_name, O_APPEND | O_RDWR | O_CREAT,0755);
        i++;
        if (i >WAIT_TIME){
            sprintf(log,"Message wasn't written because file %s couldn't be opened\n",args->output_name);
            write_to_log_file(log);
            free(args->message);
            free(args->output_name);
            free(args);
            return NULL;
        }
    }
    char* log_time = get_time_for_output_log(GPS_TIME);
    write(output_fd,log_time,strlen(log_time)); //adding timestamp into the log
    free(log_time);
   
    if(write_bytes(output_fd,args->message,args->message_size)) {
        sprintf(log,"Error while writing to the file %s\n",args->output_name);
        write_to_log_file(log);
    }
    else {
	sprintf(log,"Message was succesfully written to file %s\n",args->output_name);
	write_to_log_file(log);
    }
    close(output_fd);
    free(args->message);
    free(args->output_name);
    free(args);
    return NULL;
}

int header_check(int input_fd,unsigned char* buff,int *bytes_read){
    int i=0;
    int flag=0;//flag for checking whether the heather war read correctly or not.
    unsigned char header[]={0xff,0xff,0xc4,0xd7};
    while(1){
        flag=0;
        if(read_bytes(input_fd,buff,bytes_read,COMMAND_BYTE)) return -1;//we need to read 7 bytes, because 7th byte is a type of telegram, which is returned
        for(i=0;i<4;i++){
            if (buff[i]==header[i])
                continue;
            else {
                clear_buff(buff,bytes_read,1);
                flag=1;
                break;
            }
        }
        if(!flag)
            return buff[COMMAND_BYTE-1];
    }
}
