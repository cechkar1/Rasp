

#include "terminal.h"
#include "mainHeather.h"

void print_config(){
	char log[400];
    	sprintf(log,"Configuration:\n     port: %s\n baudrate: %d\n   FTP_IP: %s\n  logName: %s\n  logSize: %d\n  logFold: %s\n password: %s\n username: %s\n  up_time: %d\n   PCtime: %d\n\n",
        config->port,
        config->baudrate_normal,
	config->FTP_IP,
	config->logName,
	config->logSize,
	config->logFold,
        config->password,
        config->username,
	config->up_time,
        config->PCtime);
        
    	write_to_log_file(log); 
}

void fill_struct(xmlNode *cur,xmlDoc * doc){
    cur = cur->xmlChildrenNode;
    while (cur != NULL) {
        if ((!strcmp((char*)cur->name, "FTP_IP"))){
            free(config->FTP_IP);
            config->FTP_IP = (char*)xmlNodeListGetString(doc, cur->xmlChildrenNode, 1);
        }
        if ((!strcmp((char*)cur->name, "baudrate"))){
            char *tmp = (char*)xmlNodeListGetString(doc, cur->xmlChildrenNode, 1);
            int baudrate = atoi(tmp);
	    config->baudrate_normal = baudrate;
            free(tmp); //we have to free the string...
                 if(baudrate==300) config->baudrate = B300; // now we have to match a correct predefined value
            else if(baudrate==1200) config->baudrate = B1200;
            else if(baudrate==4800) config->baudrate = B4800;
            else if(baudrate==9600) config->baudrate = B9600;
            else if(baudrate==19200) config->baudrate = B19200;
            else if(baudrate==38400) config->baudrate = B38400;
            else if(baudrate==57600) config->baudrate = B57600;
            else if(baudrate==115200) config->baudrate = B115200;
            else if(baudrate==230400) config->baudrate = B230400;
            else write_to_log_file("Wrong baudrate is set. Using default...");
        }
        if ((!strcmp((char*)cur->name, "logName"))){
            free(config->logName);
            config->logName = (char*)xmlNodeListGetString(doc, cur->xmlChildrenNode, 1);
        }
         if ((!strcmp((char*)cur->name, "logSize"))){
            char *tmp = (char*)xmlNodeListGetString(doc, cur->xmlChildrenNode, 1);
            config->logSize = atoi(tmp);
            free(tmp);
        }
	if ((!strcmp((char*)cur->name, "up_time"))){
            char *tmp = (char*)xmlNodeListGetString(doc, cur->xmlChildrenNode, 1);
            config->up_time = atoi(tmp);
            free(tmp);
        }
         if ((!strcmp((char*)cur->name, "port"))){
            free(config->port);
            config->port = (char*)xmlNodeListGetString(doc, cur->xmlChildrenNode, 1);
        }
         if ((!strcmp((char*)cur->name, "username"))){
            free(config->username);
            config->username = (char*)xmlNodeListGetString(doc, cur->xmlChildrenNode, 1);
         }
	if ((!strcmp((char*)cur->name, "logFold"))){
            free(config->logFold);
            config->logFold = (char*)xmlNodeListGetString(doc, cur->xmlChildrenNode, 1);
         }
        if ((!strcmp((char*)cur->name, "password"))){
            free(config->password);
            config->password = (char*)xmlNodeListGetString(doc, cur->xmlChildrenNode, 1);
        }
        if ((!strcmp((char*)cur->name, "PCtime"))){
            char *tmp = (char*)xmlNodeListGetString(doc, cur->xmlChildrenNode, 1);
            config->PCtime = atoi(tmp);
            free(tmp);
        }
        cur = cur->next;
    }
}



void set_default_config(){
        config =(T_config*)malloc(sizeof(T_config));
        config->port=(char*)malloc(200*sizeof(char));
        strcpy(config->port,"/dev/ttyUSB0");
        config->FTP_IP=(char*)malloc(200*sizeof(char));
	strcpy(config->FTP_IP,"srva0.endora.cz");
        config->logName=(char*)malloc(50*sizeof(char));
	strcpy(config->logName,"log.txt");
        config->password=(char*)malloc(50*sizeof(char));
        strcpy(config->password,"Karel1");
	config->logFold=(char*)malloc(50*sizeof(char));
	strcpy(config->logFold,"logs");
        config->username=(char*)malloc(50*sizeof(char));
        strcpy(config->username,"rasppiop");
	config->baudrate=B19200;
	config->baudrate_normal=19200;
	config->logSize=180;
	config->up_time=10;
        config->PCtime=0;
}

char * find_config_on_flash_drive(){
    struct dirent* dent;
    char* name = (char*)malloc(200*sizeof(char));
    DIR* srcdir = opendir(FLASH_PATH);

    if (srcdir == NULL)
    {
        free(name);
        return NULL;
    }

    while((dent = readdir(srcdir)) != NULL)
    {
        struct stat st;
        strcpy(name,FLASH_PATH);
        if(strcmp(dent->d_name, ".") == 0 || strcmp(dent->d_name, "..") == 0 || strcmp(dent->d_name, "SETTINGS") == 0)
            continue;

        if (fstatat(dirfd(srcdir), dent->d_name, &st, 0) < 0)
        {
            continue;
        }

        if (S_ISDIR(st.st_mode)){
            strcat(name,dent->d_name);
            strcat(name,"/");
            strcat(name,CONFIG_FILE_NAME);
            FILE * file=fopen(name,"r");
            if (file!=NULL){
                fclose(file);
                closedir(srcdir);
                return name;
            }
        }
    }
    free(name);
    closedir(srcdir);
    return NULL;
    
}

int fill_config(char * config_file){
   
    set_default_config();
    xmlDoc *doc = NULL;
    xmlNode *root_element = NULL;

    /*
     * this initialize the library and check potential ABI mismatches
     * between the version it was compiled for and the actual shared
     * library used.
     */
    LIBXML_TEST_VERSION

    /*parse the file and get the DOM */
    char * flash_config;
    flash_config=find_config_on_flash_drive();
    if(flash_config!=NULL) {
        doc = xmlReadFile(flash_config, NULL, 0);
        char log[200];
        sprintf(log,"Using config from %s\n",flash_config);
        write_to_log_file(log);
        free(flash_config);
    }
    else doc = xmlReadFile(config_file, NULL, 0);

    if (doc == NULL) {
	write_to_log_file("Error: could not parse configuration file. Using default...\n");
        xmlCleanupParser();
	return 1;
    }
    /*Get the root element node */
    root_element = xmlDocGetRootElement(doc);

    fill_struct(root_element,doc);

    /*free the document */
    xmlFreeDoc(doc);

    /*
     *Free the global variables that may
     *have been allocated by the parser.
     */
    xmlCleanupParser();

    return 0;
}
void free_config(){
    free(config->FTP_IP);
    free(config->logName);
    free(config->port);
    free(config->username);
    free(config->password);
    free(config->logFold);
    free(config);
}
