/* 
 * File:   raspFTP.c.c
 * Author: karel
 *
 * Created on September 2, 2015, 5:21 PM
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <linux/serial.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <termios.h>
#include <stdint.h>
#include <signal.h>

#include <libxml/parser.h>
#include <libxml/tree.h>

#include "terminal.h"
#include "mainHeather.h"

#define CONFIG_FILE "raspConfig.xml"
/*
 * 
 */

T_config * config=NULL;
int signal_flag=0;
void sighandler(int signum)
{
    signal_flag=1;
}

int main(int argc, char** argv) {
    
    signal(SIGINT,sighandler);
    
    //fcntl(usb, F_SETFL, FNDELAY); // Read() returns -1 or 0 when input is empty instead of waiting, when this is set.
    fill_config(CONFIG_FILE);
    int input_fd = open (config->port, O_RDWR | O_NOCTTY | O_SYNC);
    if (input_fd < 0) {
        printf("Couldn't open the port (%s).\n",config->port);
        return errno;
    }
    
    serial_set_attributes(input_fd, config->baudrate, CS8, 1, 0, 0);
    char *buff=(char*)malloc(MAX_LOG_LENGTH*sizeof(char));
    memset(buff,0,sizeof(buff));
    
    int bytes_read=0;
    int tele_type=0;
    int check=0;
    int output_fd=-1;
    int output_size=0;
    
    while (!signal_flag){
        tele_type=header_check(input_fd,buff,&bytes_read); // recognizes which type of telegram is on the line
        if(tele_type==REQ_ID) REQ(input_fd,buff,&bytes_read);
        else if (tele_type==PASS_ID){
            printf("Telegram type PASS recognized after %d telegrams.\n",check);
            PASS(input_fd,buff,&bytes_read,&output_fd,&output_size);
        }
        else {
            printf("Unrecognized command byte - 0x%02x\n",tele_type & 0xFF);
            memmove(buff,buff+COMMAND_BYTE,(bytes_read-COMMAND_BYTE)*sizeof(*buff)); //remove unrecognized heather from the buffer
            bytes_read-=COMMAND_BYTE;
        }
        check++;
    }
    
    upload_file(output_fd,&output_size,1);
    free(buff);
    free_config();
    close(input_fd);  
    
    
//    char command[MAX_COMMAND_LENGTH]={0};
//    
//    strcat(command,"curl -T ");
//    strcat(command,config->current_fileName);
//    strcat(command," ");
//    strcat(command,config->FTP_IP);
//    strcat(command," -u ");
//    strcat(command,USR_PWD);
//
//    system(command);
//    return (EXIT_SUCCESS);
}

