#include "StdAfx.h"
#include ".\crc.h"
#using <mscorlib.dll>

using namespace SimulatorRB;

Crc::Crc(void)
{
}

Crc::~Crc(void)
{
}

Byte Crc::MakeCRC(Byte vstup[], UInt16 inic, UInt16 n)[]
{
	UInt16 pom, i=0;

	for(; n!=0; n--)
	{  // Výpočet pro n prvků
		pom = CRCTab[(Byte)((UInt16)vstup[i++] ^ inic)];
		inic = pom ^ ((inic >> 8) & 0x00FF);
	}
	inic = inic^0xFFFF;
	return(BitConverter::GetBytes(inic));  // Vypočítaná hodnota CRC
}
