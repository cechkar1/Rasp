/* 
 * File:   mainHeather.h
 * Author: karel
 *
 * Created on September 24, 2015, 12:08 PM
 */

#ifndef MAINHEATHER_H
#define	MAINHEATHER_H

#ifdef	__cplusplus
extern "C" {
#endif

#define FILE "testFile"
#define FTP_ADD "ftp://srva0.endora.cz"
#define USR_PWD ""
#define MAX_COMMAND_LENGTH 50
#define MAX_LOG_LENGTH 8192
#define MAX_MESSAGE_LENGTH 1500
    
#define PASS_HEATHER_LEN 9
#define REQ_LEN 15

int header_check(int fd,char* buff,int* bytes_read);


#ifdef	__cplusplus
}
#endif

#endif	/* MAINHEATHER_H */

