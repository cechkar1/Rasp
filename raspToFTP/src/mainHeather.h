/* 
 * File:   mainHeather.h
 * Author: karel
 *
 * Created on September 24, 2015, 12:08 PM
 */

#ifndef MAINHEATHER_H
#define	MAINHEATHER_H
#define _POSIX_C_SOURCE 200809L

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <linux/serial.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <termios.h>
#include <time.h>
#include <stdint.h>
#include <libxml/parser.h>
#include <libxml/tree.h>
#include <pthread.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <unistd.h>

#ifdef	__cplusplus
extern "C" {
#endif

#define FTP_ADD "ftp://srva0.endora.cz"
#define USR_PWD "regiosat:karel"
#define MAX_COMMAND_LENGTH 400
#define MAX_LOG_LENGTH 8192
#define MAX_MESSAGE_LENGTH 1500  
#define PASS_HEATHER_LEN 9
#define REQ_HEATHER_LEN 9
#define HEATHER_LEN 9
#define COMMAND_BYTE 7
#define MESSAGE_LEN_BYTE 8
#define SYNC_BYTES 4
#define GPS_TIME 1
    
#define FLASH_PATH "/media/"    
#define DETACHED 1
#define UNDETACHED 0
#define UPLOAD_TIME 10
#define LOG_DURATION 1
#define LOG_DIR "logs"
#define LOG_END "_STAT_LOG.txt"
    
    
#define PASS_ID 0x0B
#define REQ_ID 0x0A
#define CONFIG_FILE_NAME "raspConfig.xml"
#define CONFIG_FILE "/home/pi/raspConfig.xml"
#define WAIT_TIME 3

    

    

typedef struct write_to_args{
    char * output_name;
    int message_size;
    unsigned char *message;
}T_args; 
    

typedef struct t_config{
        char * port;
	int baudrate;
	char *FTP_IP;
	char * logName;
	int logSize;
        char * username;
        char * password;
	char * logFold;
	int up_time;
	int baudrate_normal;
        int PCtime;
    }T_config;

    
int check_time(time_t * timer,int period);    
    
/**
 * @brief Creates a CRC16 of first n bytes in input, by using a lookup table of polynomial x16 x12 x5 x1
 * 
 * @param input an array with bytes
 * @param n amount of bytes
 * @return two bytes of CRC16
 */
unsigned char * get_CRC16(unsigned char *input,int n);
/**
 * @brief controls if the block of data has the same checksum which was sent with them.
 * 
 * The checksum is calculated and compared with next two bytes of the data block
 * 
 * @param message an array with bytes
 * @param size amount of bytes
 * @return 0. 1 when unsuccessful
 */
int check_CRC16(unsigned char *message,int size);

/**
 * @brief Creates a string in format 'YYMMDD_HHMMSS<name_end>'.
 * 
 * Depending on switch gps_time, the function takes time from a system or from a GPS device connected through serial port
 * 
 * @param gps_time a switch between using time from a system or GPS
 * @param name_end  string witch will be added to the behind timestamp
 * @return created string (file name)
 */
char * create_output_name(char * name_end);
/**
 * @brief Creates a string in format 'YYYY-MM-DD_HH:MM:SS'.
 * 
 * Depending on switch gps_time, the function takes time from a system or from a GPS device connected through serial port
 * 
 * @param gps_time a switch between using time from a system or GPS
 * @return created string (a timestamp)
 */
char * get_time_for_output_log();
/**
 * @brief creates a system command and executes it
 * @param upload_name name of the file to be uploaded
 * @return 1 when the file wasn't uploaded successfully
 */
int use_curl_command(char * upload_name);
/**
 * @brief Converts bytes from the buffer and writes them to the output file as a text with "-" between and newline at the end
 * @param output_fd file descriptor of a file where the text is writtend
 * @param message buffer
 * @param amount how many bytes is converted
 * @return 1 when unsuccessful
 */
int write_bytes(int output_fd,unsigned char * message,int amount);
/**
 * @brief Converts bytes from the buffer and writes them to the output file
 *  as a text with "-" between and newline at the end and adds timestamp at the beginning
 * @param output_fd file descriptor of a file where the text is written
 * @param message buffer
 * @param amount how many bytes is converted
 * @return 1 when unsuccessful
 */
void* write_to_output(void* args);

/**
 * @brief Clears buffer - shift bytes by amount spaces and reduces bytes_read
 * @param buff
 * @param bytes_read
 * @param amount
 */
void clear_buff(unsigned char *buff,int* bytes_read, int amount);
/**
 * @brief Read an amount of bytes from input_fd
 * @param input_fd
 * @param buff
 * @param bytes_read
 * @param amount
 * @return 1 when signal_flag is set
 */
int read_bytes(int input_fd,unsigned char * buff, int* bytes_read,int amount);

/**
 * @brief searches in bytes received from input for heather and when successful returns a COMMAND_BYTE
 * @param input_fd
 * @param buff
 * @param bytes_read
 * @return COMMAND_BYTE
 */
int header_check(int input_fd,unsigned char* buff,int *bytes_read);
/**
 * @brief Checks message length bytes and reads the rest of the message on input +2 bytes of CRC16
 * This is where the CRC16 is checked ass well
 * @param input_fd
 * @param buff
 * @param bytes_read
 * @return size of the relevant message (which was in message length bytes), -1 when the CRC doesn't match
 */
int read_rest_of_message(int input_fd,unsigned char * buff, int * bytes_read);

/**
 * @brief Handles and responds to REQ telegram
 * @param input_fd
 * @param buff
 * @param bytes_read
 */
void REQ(int input_fd,unsigned char *buff,int* bytes_read);
/**
 * @brief Handles and responds a PASS telegram - writes its content to the file
 * @param input_fd
 * @param buff
 * @param bytes_read
 * @param output_fd
 */
void PASS(int input_fd,unsigned char *buff,int* bytes_read);
/**
 * @Closes currently opened file, uploads it to FTP server and creates a new file
 * @param output_fd
 * @param last when this isn't set to 0, any new file isn't opened.
 * @return 1 when unsuccessful
 */
int upload_file(char * upload_name,int detached);

/**
 * @brief Fills config from xml configuration file
 * @param config_file
 * @return 
 */
void fill_struct(xmlNode *cur,xmlDoc * doc);

/**
 * @brief Fills config with default values
 */
void set_default_config();

/**
 * @brief Fills config from xml configuration file
 * @param config_file
 * @return 
 */
int fill_config(char* config_file);
/**
 * @brief Frees allocated memory in config
 */
void free_config();


/**
 * @brief sets signal flag to 1
 * @param signum
 */
void sighandler(int signum);
/**
 * @brief Sets global variable 'global_time' to a current time (represented by a string) from a NMEA sentence from attached GPS (from port ttyAMA0)
 */
void * getTimeFromGPS(void* v);
/**
 * @brief thread with 'main' function of the program
 */
void * thread_main_func(void* nothing);
/**
 * @brief gets data directly from GSM and puts them to a log file .GSM
 */
void * getDataFromGSM(void* v);
/**
 * @brief writes 'text' to the log file
 */
void write_to_log_file(char * text);
/**
 * @brief prints current config
 */
void print_config();


//structure which holds current config
extern T_config * config;
//name of a file with logs of runtime
extern char * g_err_log;
//signal flag to end the program correctly
extern int volatile g_signal_flag;
//name of a current file where data is saved
extern char * g_current_output_name;
//time represented by string
extern char global_time[13]; //12+1 for \0 format "HHMMSSYYmmDD"
//mutex lock
extern pthread_mutex_t mutex_lock;


#ifdef	__cplusplus
}
#endif

#endif	/* MAINHEATHER_H */

