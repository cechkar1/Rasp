/**
 * @file     raspFTP.c
 *
 * @brief    Main file 
 * 
 * @author   Karel Cech
 * @version  0p0
 * @date     2015
 * 
 * Created on November 2, 2015, 5:21 PM
 */

#include "terminal.h"
#include "mainHeather.h"


/*
 * 
 */

T_config * config=NULL;
int volatile g_signal_flag=0;
char * g_err_log=NULL;
char * g_current_output_name=NULL;
char global_time[]="HHMMSSDDmmYY"; //12+1 for \0 "HHMMSSDDmmYY"
pthread_mutex_t mutex_lock;


int main(int argc, char** argv) {
    //setup signals
    struct sigaction sigact;
    sigemptyset(&sigact.sa_mask);
    sigact.sa_flags   = SA_SIGINFO;
    sigact.sa_handler = sighandler;
    sigaction(SIGINT, &sigact, 0);
    sigaction(SIGTERM,&sigact,0);
    sigaction(SIGQUIT,&sigact,0);
    //start thread for getting time from GPS
    pthread_t time_thread;
    pthread_create(&time_thread, NULL, getTimeFromGPS, (void *)NULL);
    //wait for time to initialize
    sleep(2);
    //fill config from a config file
    fill_config(CONFIG_FILE);
    g_err_log=create_output_name(LOG_END);
    sleep(1);
    g_current_output_name=create_output_name(config->logName);
    print_config();
    //start thread for checking data directly from GSM
    pthread_t gsm;
    pthread_create(&gsm,NULL,getDataFromGSM,(void*)NULL);
    //start thread for reading data from JNAR reformating them and sending to FTP server
    pthread_t main_func;
    pthread_create(&main_func, NULL,thread_main_func, (void *)NULL);
    char n=0;
//update.rc.d udev enable
    while (n != 'q' && g_signal_flag!=1){
	clearerr(stdin);
	scanf("%c",&n);
    }
    pthread_kill(main_func,SIGUSR1);
    pthread_kill(time_thread,SIGUSR1);
    pthread_kill(gsm,SIGUSR1);
    pthread_join(main_func,NULL);
    pthread_join(time_thread,NULL);
    pthread_join(gsm,NULL);

    ///upload the last file
    upload_file(g_current_output_name,UNDETACHED);
    free_config();
    free(g_err_log);
    free(g_current_output_name);
    pthread_exit(NULL);
}
