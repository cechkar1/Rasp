#include "terminal.h"
#include "mainHeather.h"

    /**
 * @brief CRC16 look up table for polynomial x16 x12 x5 x1
 */
    static uint16_t CRCTab[] = { 
			0x0000, 0x1189, 0x2312, 0x329b, 0x4624, 0x57ad, 0x6536, 0x74bf,
			0x8c48, 0x9dc1, 0xaf5a, 0xbed3, 0xca6c, 0xdbe5, 0xe97e, 0xf8f7,
			0x1081, 0x0108, 0x3393, 0x221a, 0x56a5, 0x472c, 0x75b7, 0x643e,
			0x9cc9, 0x8d40, 0xbfdb, 0xae52, 0xdaed, 0xcb64, 0xf9ff, 0xe876,
			0x2102, 0x308b, 0x0210, 0x1399, 0x6726, 0x76af, 0x4434, 0x55bd,
			0xad4a, 0xbcc3, 0x8e58, 0x9fd1, 0xeb6e, 0xfae7, 0xc87c, 0xd9f5,
			0x3183, 0x200a, 0x1291, 0x0318, 0x77a7, 0x662e, 0x54b5, 0x453c,
			0xbdcb, 0xac42, 0x9ed9, 0x8f50, 0xfbef, 0xea66, 0xd8fd, 0xc974,
			0x4204, 0x538d, 0x6116, 0x709f, 0x0420, 0x15a9, 0x2732, 0x36bb,
			0xce4c, 0xdfc5, 0xed5e, 0xfcd7, 0x8868, 0x99e1, 0xab7a, 0xbaf3,
			0x5285, 0x430c, 0x7197, 0x601e, 0x14a1, 0x0528, 0x37b3, 0x263a,
			0xdecd, 0xcf44, 0xfddf, 0xec56, 0x98e9, 0x8960, 0xbbfb, 0xaa72,
			0x6306, 0x728f, 0x4014, 0x519d, 0x2522, 0x34ab, 0x0630, 0x17b9,
			0xef4e, 0xfec7, 0xcc5c, 0xddd5, 0xa96a, 0xb8e3, 0x8a78, 0x9bf1,
			0x7387, 0x620e, 0x5095, 0x411c, 0x35a3, 0x242a, 0x16b1, 0x0738,
			0xffcf, 0xee46, 0xdcdd, 0xcd54, 0xb9eb, 0xa862, 0x9af9, 0x8b70,
			0x8408, 0x9581, 0xa71a, 0xb693, 0xc22c, 0xd3a5, 0xe13e, 0xf0b7,
			0x0840, 0x19c9, 0x2b52, 0x3adb, 0x4e64, 0x5fed, 0x6d76, 0x7cff,
			0x9489, 0x8500, 0xb79b, 0xa612, 0xd2ad, 0xc324, 0xf1bf, 0xe036,
			0x18c1, 0x0948, 0x3bd3, 0x2a5a, 0x5ee5, 0x4f6c, 0x7df7, 0x6c7e,
			0xa50a, 0xb483, 0x8618, 0x9791, 0xe32e, 0xf2a7, 0xc03c, 0xd1b5,
			0x2942, 0x38cb, 0x0a50, 0x1bd9, 0x6f66, 0x7eef, 0x4c74, 0x5dfd,
			0xb58b, 0xa402, 0x9699, 0x8710, 0xf3af, 0xe226, 0xd0bd, 0xc134,
			0x39c3, 0x284a, 0x1ad1, 0x0b58, 0x7fe7, 0x6e6e, 0x5cf5, 0x4d7c,
			0xc60c, 0xd785, 0xe51e, 0xf497, 0x8028, 0x91a1, 0xa33a, 0xb2b3,
			0x4a44, 0x5bcd, 0x6956, 0x78df, 0x0c60, 0x1de9, 0x2f72, 0x3efb,
			0xd68d, 0xc704, 0xf59f, 0xe416, 0x90a9, 0x8120, 0xb3bb, 0xa232,
			0x5ac5, 0x4b4c, 0x79d7, 0x685e, 0x1ce1, 0x0d68, 0x3ff3, 0x2e7a,
			0xe70e, 0xf687, 0xc41c, 0xd595, 0xa12a, 0xb0a3, 0x8238, 0x93b1,
			0x6b46, 0x7acf, 0x4854, 0x59dd, 0x2d62, 0x3ceb, 0x0e70, 0x1ff9,
			0xf78f, 0xe606, 0xd49d, 0xc514, 0xb1ab, 0xa022, 0x92b9, 0x8330,
			0x7bc7, 0x6a4e, 0x58d5, 0x495c, 0x3de3, 0x2c6a, 0x1ef1, 0x0f78
		};
    

int check_time(time_t* timer,int period){
    time_t curr_time;
    time (&curr_time);
    if ((int)(curr_time-*timer)>period)
        return 1;
    else return 0;
}

unsigned char * get_CRC16(unsigned char *input,int n){
	uint16_t pom=0, i=0;
	uint16_t inic = 0xffff;
	for(; i < n; i++){
            input[i]&=0xFF;
            pom = CRCTab[(uint8_t)((uint16_t)input[i] ^ inic)];
            inic = pom ^ ((inic >> 8) & 0x00FF);
	}
	inic = inic^0xFFFF;
        unsigned char * crc = (unsigned char*)malloc(2*sizeof(unsigned char));
        crc[0]= inic; //we have to switch the bytes
        crc[1]= inic >> 8;
        return crc;
}


int check_CRC16(unsigned char *message,int size){
    unsigned char * crc = get_CRC16(message,size); //two bytes with CRC
    if (*(message + size)==crc[0] && *(message + size+1)==crc[1]) {
        //printf("Good CRC16 %02x%02x\n",crc[0]& 0xFF,crc[1]& 0xFF); //we have to & chars with 0xFF everytime because of reasons... without it it sometimes prints "ffffffAB" instead of AB.
        free(crc);
        return 0;
    }
    else {
        char log [200];
        sprintf(log,"Wrong CRC16 %02x%02x expected - %02x%02x\n",crc[0]& 0xFF,crc[1]& 0xFF,*(message + size)& 0xFF,*(message + size+1)& 0xFF);
        write_to_log_file(log);
        free(crc);
        return 1;
    }
} 


void clear_buff(unsigned char *buff,int* bytes_read, int amount){
    memmove(buff,buff + amount,(*bytes_read-amount)*sizeof(*buff));
    *bytes_read-=amount;
}

int read_bytes(int input_fd,unsigned char * buff, int* bytes_read,int amount){
        while (*bytes_read < amount){ // we need to know the data length first
            *bytes_read+=read(input_fd,buff+*bytes_read,MAX_LOG_LENGTH-*bytes_read);
            if(g_signal_flag) return 1;
        }
        return 0;
}

int read_rest_of_message(int input_fd, unsigned char * buff, int * bytes_read){
    if (read_bytes(input_fd,buff,bytes_read,HEATHER_LEN)) return -1; //first we need to know the data length in the message
    unsigned int message_size = buff[MESSAGE_LEN_BYTE-1];
    message_size = message_size << 8;
    message_size |= buff[MESSAGE_LEN_BYTE]; 
    if (message_size > MAX_MESSAGE_LENGTH) { //Documentation says, that maximum is 1472 bytes - we don't want to overflow
        clear_buff(buff,bytes_read,HEATHER_LEN); //we have to skip the heather (and the two bytes of length)  only, there may be valid data after it
        return -1;
    }
    if (read_bytes(input_fd,buff,bytes_read,message_size+2+HEATHER_LEN)) return -1; //+2 for CRC16
    //printf("bytes read %d, message_size %d\n",*bytes_read,message_size);
    
    if(check_CRC16(buff+SYNC_BYTES,message_size+HEATHER_LEN-SYNC_BYTES)){ //last five bytes of Heather are part of CRC16 calculation       
        clear_buff(buff,bytes_read,HEATHER_LEN); //we have to skip the heather (and the two bytes of length)  only, there may be valid data after it
        return -1;
    }
    else return message_size;
}


void REQ(int input_fd,unsigned char *buff,int* bytes_read){ //this program counts with slave not having any message for master -> we don't have to check CRC16 here
    unsigned char answer[]={0xff,0xff,0xc4,0xd7,0x00,0x01,0x8a,0x00,0x00,0x5a,0xac};
    answer[5]=buff[4]; //putting a slave ID into the answer.
    //computing the checksum for answer and putting it to it's end;
    unsigned char * crc = get_CRC16(answer+SYNC_BYTES,5);
    answer[sizeof(answer)-2]=crc[0];
    answer[sizeof(answer)-1]=crc[1];
    free(crc);

    //load rest of the message to buffer and check the CRC16
    int message_length=read_rest_of_message(input_fd,buff,bytes_read);
    if(message_length<0)return;
    write (input_fd,answer,11);
    
    clear_buff(buff,bytes_read,message_length+2+HEATHER_LEN);
}


void PASS(int input_fd,unsigned char *buff,int* bytes_read){
    unsigned char answer[]={0xff,0xff,0xc4,0xd7,0x00,0x01,0x8b,0x00,0x00,0x86,0xf6};
    answer[5]=buff[4];//putting a slave ID into the answer
    //computing the checksum for answer
    unsigned char * crc = get_CRC16(answer+SYNC_BYTES,5);
    answer[sizeof(answer)-2]=crc[0];
    answer[sizeof(answer)-1]=crc[1];
    free(crc);
    
    //load rest of the message to buffer and check the CRC16
    int message_length=read_rest_of_message(input_fd,buff,bytes_read);
    //if the crc doesn't match - return
    if(message_length<0)return;
    write(input_fd,answer,11); //answering...
    
    //copying buffer and output name to pass them to a new thread
    char * output_name=(char*)malloc(400*sizeof(char));
    strcpy(output_name,g_current_output_name);
    unsigned char * message=(unsigned char*)malloc(message_length*sizeof(unsigned char));
    memcpy(message,buff+HEATHER_LEN,message_length);
    clear_buff(buff,bytes_read,message_length+2+HEATHER_LEN); //+2for CRC16
    
    //initialization of a new thread
    T_args * args=(T_args*)malloc(sizeof(T_args));
    args->message=message;
    args->message_size=message_length;
    args->output_name=output_name;
    pthread_t write_to_output_thread;
    pthread_create(&write_to_output_thread,NULL,write_to_output,(void*)args);
    pthread_detach(write_to_output_thread);
    //cant free message and output name, because it may be still usable in write_to_output_thread - need to free it there
}
