﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jnarPolLog
{
  class ByteDataQueue
  {
    private ByteData[] arrayByteData;
    private UInt16 sizeQueue, writeOffs, readOffs, countData;

    //parametry konstruktoru - max pocet zaznamu ve fronte a max velikost kazdeho zanzamu
    public ByteDataQueue(UInt16 _sizeQueue, UInt16 _sizeData)
    {
      sizeQueue = _sizeQueue;
      writeOffs = 0;
      readOffs = 0; 
      countData = 0;
      //vytvoreni pole
      arrayByteData = new ByteData[_sizeQueue];
      //UInt16 x = 0;
      //for(x = 0;x < _sizeQueue;x++)
      //{
      //  arrayByteData[x] = new ByteData(_sizeData);
      //}
    }

    //volat pred ctenim
    private void incRead()
    {
      readOffs++;
      if(readOffs == sizeQueue)
      {
        readOffs = 0;
      }

//      countData--;
    }

    //volat pred zapisem
    private void incWrite()
    {
      writeOffs++;
      if(writeOffs == sizeQueue)
      {
        writeOffs = 0;
      }

//      countData++;

      if(writeOffs == readOffs)
      {
        countData = 0;
        throw new IndexOutOfRangeException("Fronta dat je plna");
      }
    }


    //nakopiruje data do fronty z reference 
    public void AddData(ref ByteData _data)
    {
      this.incWrite();
      arrayByteData[writeOffs] = new ByteData(_data.GetSizeData()); //vytvoreni radku
      arrayByteData[writeOffs].CopyAllFrom(ref _data);  
      countData++; //aktualizovat az po celem zapisu
    }

    //nakopiruje data do fronty z reference, na zacatku odsadi o offset
    public void AddData(ref ByteData _data, UInt16 _dstOffs)
    {
      this.incWrite();
      arrayByteData[writeOffs] = new ByteData(Convert.ToUInt16(_data.GetSizeData() + _dstOffs)); //vytvoreni radku
      arrayByteData[writeOffs].CopyToOffsDataFrom(ref _data, _dstOffs);
      countData++; //aktualizovat az po celem zapisu
    }

    //nakopiruje subdata do fronty z reference
    public void AddSubData(ref ByteData _data, UInt16 _offset, UInt16 _size)
    {
      this.incWrite();
      arrayByteData[writeOffs] = new ByteData(_size); //vytvoreni radku
      arrayByteData[writeOffs].CopySubDataFrom(ref _data, _offset, _size);
      countData++; //aktualizovat az po celem zapisu
    }

    //nakopiruje subdata do fronty z reference
    public void AddSubData(ref byte[] _data, UInt16 _offset, UInt16 _size)
    {
      this.incWrite();
      arrayByteData[writeOffs] = new ByteData(_size); //vytvoreni radku
      arrayByteData[writeOffs].CopySubDataFrom(ref _data, _offset, _size);
      countData++; //aktualizovat az po celem zapisu
    }

    //nakopiruje subdata do fronty z reference,  na zacatku odsadi o offset
    public void AddSubData(ref ByteData _data, UInt16 _srcOffset, UInt16 _size, UInt16 _dstOffs)
    {
      this.incWrite();
      arrayByteData[writeOffs] = new ByteData(Convert.ToUInt16(_size + _dstOffs)); //vytvoreni radku
      arrayByteData[writeOffs].CopyToOffsSubDataFrom(ref _data, _srcOffset, _size, _dstOffs);
      countData++; //aktualizovat az po celem zapisu
    }

    //nakopiruje subdata do fronty z reference,  na zacatku odsadi o offset
    public void AddSubData(ref byte[] _data, UInt16 _srcOffset, UInt16 _size, UInt16 _dstOffs)
    {
      this.incWrite();
      arrayByteData[writeOffs] = new ByteData(Convert.ToUInt16(_size + _dstOffs)); //vytvoreni radku
      arrayByteData[writeOffs].CopyToOffsSubDataFrom(ref _data, _srcOffset, _size, _dstOffs);
      countData++; //aktualizovat az po celem zapisu
    }

    //nakopirujde data z fronty do data a ve fronte je pak smaze
    public void GetData(ref ByteData _data)
    {
      if(writeOffs != readOffs)
      {
        this.incRead();
        _data.CopyAllFrom(ref arrayByteData[readOffs]);
        countData--; //aktualizovat az po vytahnuti
      }
    }

    //nakopirujde data z fronty do data od pozadovane offsetu a ve fronte je pak smaze
    public void GetDataToOffs(ref ByteData _data, UInt16 _dtsOffs)
    {
      if(writeOffs != readOffs)
      {
        this.incRead();
        _data.CopyToOffsDataFrom(ref arrayByteData[readOffs], _dtsOffs);
        countData--; //aktualizovat az po vytahnuti
      }
    }

    public UInt16 GetCountData()
    {
      return countData;
    }

    //vrati true, kdyz je fronta plna
    public bool IsFull()
    {
      bool result = false;

      int nextWrite = writeOffs + 1;
      if(nextWrite < sizeQueue)
      {
        if(nextWrite == readOffs)
        {
          result = true;
        }
      }
      else
      {
        if(readOffs == 0) //preteklo pres velikost, tak staci test na nula
        {
          result = true;
        }
      }

      return result;
    }

    //vrati true, kdyz je fronta prazdna
    public bool IsEmpty()
    {
      bool result = false;

      if(countData == 0)
      {
        result = true;
      }

      return result;
    }



  }
}
