#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <linux/serial.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <termios.h>
#include <time.h>
#include <stdint.h>
#include <libxml/parser.h>
#include <libxml/tree.h>
#include <pthread.h>
#include <signal.h>

#include "terminal.h"
#include "mainHeather.h"

#define GSM_SERIAL "/dev/ttyUSB1"
#define BAUDRATE B19200

void * getDataFromGSM(void* v){
        struct sigaction sigact;
        sigemptyset(&sigact.sa_mask);
        sigact.sa_flags   = SA_SIGINFO;
        sigact.sa_handler = sighandler;
        sigaction(SIGUSR1, &sigact, 0);
        
	char head[]="SISR: 4, ";
	char znak[1];
	int i=0,flag=0;
        int input_fd = open (GSM_SERIAL, O_RDWR | O_NOCTTY | O_SYNC);
	serial_set_attributes(input_fd, BAUDRATE, CS8, 1, 0, 0);
        int message_length;
        char * log=create_output_name("_GSM.GSM");
	FILE * output_fp;
	while (g_signal_flag==0){
                if (input_fd <0){
                    printf("Couldn't open GSM port\n");
                    free(log);
                    return NULL;
                }         
		read(input_fd,znak,1);
                flag=0;
		//messages begins with '^'
		if(znak[0]=='^'){
                        //loop for checking whether the head is 'SISR: 4, ' or not
			for (i=0;i<strlen(head);i++){
				read(input_fd,znak,1);
				if(znak[0]!=head[i]){
					flag=1;
					break;
				}
			}
                        if(!flag){
                            read(input_fd,znak,1);
			    //if the message is empty - continue
			    if(znak[0]=='0') continue;
			    //get the length of the message
                            message_length=(znak[0]-'0')*10;
                            read(input_fd,znak,1);
                            message_length+=znak[0]-'0'+10; // there will be 10 more bytes around the message
			    int m;
                            //need to skip some unimportant bytes (IP adress etc.). There are now 23 of them (but there could be more or less, depending on the IP address) (maybe we could just look for a newline?) for sure im skipping just 18
			    for (m=0;m<18;m++) read(input_fd,znak,1);
			    output_fp=fopen(log,"a");
			    if (output_fp==NULL){
           			 printf("couldn't open GSM log file\n");
                                 free(log);
          			 return NULL;
       			    }
                            char * logtime = get_time_for_output_log();
                            fprintf(output_fp,"\n%s",logtime);
			    free(logtime);
			    //formating bytes to an ASCII representation
                            for(i=0;i<message_length;i++){
                                    read(input_fd,znak,1);
                                    fprintf(output_fp,"%02x-",znak[0] & 0x00FF);
                            }
                            fclose(output_fp);
			    write_to_log_file("gsm message written\n");
                        }
		}
	
	}
	close(input_fd);
	free(log);
	return NULL;
}
