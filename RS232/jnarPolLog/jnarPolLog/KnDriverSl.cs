﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Windows.Forms;

namespace jnarPolLog
{
  class KnDriverSl
  {
    private Timer timerKn;                //casovac KN
    private SerialDataKn knSerial;       //seriove rozhranni
    private byte addressKn, addressSlave;  //adresy karty Kn a dotazovaneho slave
    private ByteData dataACKnData;             //pole pro zpravu ACK
    private ByteData dataRPASS;            //pole pro zpravu RPASS
    private UInt16 waitReply = 0;         //pocet cekani na odpoved
    private ByteData dataRx;             //pole pro prijem dat od slave
    private ByteDataQueue queueTx;      //fronta dat k odeslani
    private ByteData dataTx;             //pole pro odesilani dat
    private ByteDataQueue queueRx;       //fronta na ukladani vyriznutych dat od slave
    private TelegramCount tlgCount = new TelegramCount(0,0,0,0,0,0);      //pocitadlo zprav
    private bool isActMaster = false;      //priznak jestli je aktivni master
    private BlockKnMsg blockMsg = new BlockKnMsg(false, false, false, false, false, false);

    private UInt16[] tabCrc = {
      0x0000, 0x1189, 0x2312, 0x329b, 0x4624, 0x57ad, 0x6536, 0x74bf,
      0x8c48, 0x9dc1, 0xaf5a, 0xbed3, 0xca6c, 0xdbe5, 0xe97e, 0xf8f7,
      0x1081, 0x0108, 0x3393, 0x221a, 0x56a5, 0x472c, 0x75b7, 0x643e,
      0x9cc9, 0x8d40, 0xbfdb, 0xae52, 0xdaed, 0xcb64, 0xf9ff, 0xe876,
      0x2102, 0x308b, 0x0210, 0x1399, 0x6726, 0x76af, 0x4434, 0x55bd,
      0xad4a, 0xbcc3, 0x8e58, 0x9fd1, 0xeb6e, 0xfae7, 0xc87c, 0xd9f5,
      0x3183, 0x200a, 0x1291, 0x0318, 0x77a7, 0x662e, 0x54b5, 0x453c,
      0xbdcb, 0xac42, 0x9ed9, 0x8f50, 0xfbef, 0xea66, 0xd8fd, 0xc974,
      0x4204, 0x538d, 0x6116, 0x709f, 0x0420, 0x15a9, 0x2732, 0x36bb,
      0xce4c, 0xdfc5, 0xed5e, 0xfcd7, 0x8868, 0x99e1, 0xab7a, 0xbaf3,
      0x5285, 0x430c, 0x7197, 0x601e, 0x14a1, 0x0528, 0x37b3, 0x263a,
      0xdecd, 0xcf44, 0xfddf, 0xec56, 0x98e9, 0x8960, 0xbbfb, 0xaa72,
      0x6306, 0x728f, 0x4014, 0x519d, 0x2522, 0x34ab, 0x0630, 0x17b9,
      0xef4e, 0xfec7, 0xcc5c, 0xddd5, 0xa96a, 0xb8e3, 0x8a78, 0x9bf1,
      0x7387, 0x620e, 0x5095, 0x411c, 0x35a3, 0x242a, 0x16b1, 0x0738,
      0xffcf, 0xee46, 0xdcdd, 0xcd54, 0xb9eb, 0xa862, 0x9af9, 0x8b70,
      0x8408, 0x9581, 0xa71a, 0xb693, 0xc22c, 0xd3a5, 0xe13e, 0xf0b7,
      0x0840, 0x19c9, 0x2b52, 0x3adb, 0x4e64, 0x5fed, 0x6d76, 0x7cff,
      0x9489, 0x8500, 0xb79b, 0xa612, 0xd2ad, 0xc324, 0xf1bf, 0xe036,
      0x18c1, 0x0948, 0x3bd3, 0x2a5a, 0x5ee5, 0x4f6c, 0x7df7, 0x6c7e,
      0xa50a, 0xb483, 0x8618, 0x9791, 0xe32e, 0xf2a7, 0xc03c, 0xd1b5,
      0x2942, 0x38cb, 0x0a50, 0x1bd9, 0x6f66, 0x7eef, 0x4c74, 0x5dfd,
      0xb58b, 0xa402, 0x9699, 0x8710, 0xf3af, 0xe226, 0xd0bd, 0xc134,
      0x39c3, 0x284a, 0x1ad1, 0x0b58, 0x7fe7, 0x6e6e, 0x5cf5, 0x4d7c,
      0xc60c, 0xd785, 0xe51e, 0xf497, 0x8028, 0x91a1, 0xa33a, 0xb2b3,
      0x4a44, 0x5bcd, 0x6956, 0x78df, 0x0c60, 0x1de9, 0x2f72, 0x3efb,
      0xd68d, 0xc704, 0xf59f, 0xe416, 0x90a9, 0x8120, 0xb3bb, 0xa232,
      0x5ac5, 0x4b4c, 0x79d7, 0x685e, 0x1ce1, 0x0d68, 0x3ff3, 0x2e7a,
      0xe70e, 0xf687, 0xc41c, 0xd595, 0xa12a, 0xb0a3, 0x8238, 0x93b1,
      0x6b46, 0x7acf, 0x4854, 0x59dd, 0x2d62, 0x3ceb, 0x0e70, 0x1ff9,
      0xf78f, 0xe606, 0xd49d, 0xc514, 0xb1ab, 0xa022, 0x92b9, 0x8330,
      0x7bc7, 0x6a4e, 0x58d5, 0x495c, 0x3de3, 0x2c6a, 0x1ef1, 0x0f78
   };


    //parametry konstruktoru jsou ip adressa karty kn, cislo dotazovaneho slave, port a prenosovka 
    //a jmeno zapisovane pri logu dat, vychozi nstavei logovani
    public KnDriverSl(byte _addressSlave, string _port, int _baudRate, LogFileParams _logParams)
    {
      timerKn = new Timer();
      knSerial = new SerialDataKn(_port, _baudRate, _logParams);
      this.timerKn.Tick += new System.EventHandler(this.timerKn_Tick);
      timerKn.Interval = 50;
      addressKn = 0; //vzdy je nula
      addressSlave = _addressSlave;
      dataACKnData = new ByteData(7);
      dataRPASS = new ByteData(7);
      dataRx = new ByteData(1500);
      dataTx = new ByteData(1500);
      queueRx = new ByteDataQueue(5, 1500);
      queueTx = new ByteDataQueue(5, 1500);
      this.setAckNData(); //naplni se pole REQ
      this.setRPASS(); //naplni se pole RACK

      timerKn.Start(); //vse nastaveno, start driveru

    }

    private UInt16 getCrc(ref ByteData _data, UInt16 _offset, UInt16 _size)
    {
      UInt16 crc = 0xFFFF; 							//pocatecni hodnota
      UInt16 x = 0;

      for(x = 0;x < _size;x++) 		//vypocet crc
      {
        UInt16 tempCrc = System.Convert.ToUInt16((crc ^ _data.GetUi8(System.Convert.ToUInt16(_offset + x))) & 0x00FF);
        crc = System.Convert.ToUInt16((crc >> 8) ^ tabCrc[tempCrc]);
      }

      crc ^= 0xFFFF;      //xor vysledku

      return crc;

    }

    private void setAckNData()
    {
      dataACKnData.SetByte(0, addressKn);
      dataACKnData.SetByte(1, addressSlave);
      dataACKnData.SetByte(2, 0x8A);  //typ telegramu
      dataACKnData.SetByte(3, 0);    //delka dat
      dataACKnData.SetByte(4, 0);    //delka dat
      UInt16 tempCrc = this.getCrc(ref dataACKnData, 0, 5);
      dataACKnData.SetUi16(5, tempCrc, false);
    }

    private void setRPASS()
    {
      dataRPASS.SetByte(0, addressKn);
      dataRPASS.SetByte(1, addressSlave);
      dataRPASS.SetByte(2, 0x8B); //typ telegramu
      dataRPASS.SetByte(3, 0);   // delka dat
      dataRPASS.SetByte(4, 0);   //delka dat
      UInt16 tempCrc = this.getCrc(ref dataRPASS, 0, 5);
      dataRPASS.SetUi16(5, tempCrc, false);
    }

    private void timerKn_Tick(object sender, EventArgs e)
    {
      //ceka se na prijem cehokoliv
      waitReply++;

      if(knSerial.isRcvData() == true)
      {
        //prisla data
        isActMaster = true;
        waitReply = 0;

        knSerial.GetRcvData(dataRx);

        //kontrola jestli jsou spravne adresy + crc
        UInt16 tempSizeData = System.Convert.ToUInt16(dataRx.GetSizeData() - 2); // -2B crc
        UInt16 tempCrc = this.getCrc(ref dataRx, 0, tempSizeData);
        UInt16 tempRxCrc = dataRx.GetUi16(tempSizeData, false);
        if(tempCrc == tempRxCrc)
        {
          //crc ok kontrola adres
          if((dataRx.GetUi8(1) == addressKn) && (dataRx.GetUi8(0) == addressSlave))
          {
            //kontrola co je to za typ a podle toho log + pripadne ulozeni dat
            if(dataRx.GetUi8(2) == 0x0A) //prislo REQ
            {
              //zatim neumime posilat data, tak jen odpoved ACK bez dat
              if(blockMsg.BlockREQ == false)
              {
                tlgCount.countREQ++;

                if(blockMsg.BlockACK == false)
                {
                  //odpoved odeslat jen tehdy, kdzy neni nic dalsiho od mastra ve fronte 
                  //mohlo dojit ke zpozdeni vytahovani dat z driveru....
                  if(knSerial.isRcvData() == false) 
                  {
                    knSerial.SendData(ref dataACKnData);
                    tlgCount.countACK++;
                  }
                }
                else
                {
                  blockMsg.BlockACK = false;
                  //logFile.LogText(logName + " blocked ACK");
                }
              }
              else
              {
                blockMsg.BlockREQ = false;
                //logFile.LogText(logName + " blocked REQ");
              }

            }
            else if(dataRx.GetUi8(2) == 0x0B) //prislo PASS - vytahnout data a odpovedet RPASS
            {
              if(blockMsg.BlockPASS == false)
              {                
                tlgCount.countPASS++;
                //prisla data, vytahnout datovou oblast a poslat rpass
                tempSizeData = dataRx.GetUi16(3, true);
                if(tempSizeData > 0)
                {
                  queueRx.AddSubData(ref dataRx, 5, tempSizeData); //na offsetu 5 zacina datova oblast
                }
                
                if(blockMsg.BlockRPASS == false)
                {
                  //odpoved odeslat jen tehdy, kdzy neni nic dalsiho od mastra ve fronte 
                  //mohlo dojit ke zpozdeni vytahovani dat z driveru....
                  if(knSerial.isRcvData() == false)
                  {
                    knSerial.SendData(ref dataRPASS);
                    tlgCount.countRPASS++;
                  }
                }
                else
                {
                  blockMsg.BlockRPASS = false;
                  //logFile.LogText(logName + " blocked RPASS");
                }
              }
              else
              {
                blockMsg.BlockPASS = false;
                //logFile.LogText(logName + " blocked PASS");
              }
            }
            else if(dataRx.GetUi8(2) == 0x0F)
            {
              //STS netreba nic delat asi
            }
            else
            {
              //logFile.LogData(logName + " unknow type rx", ref dataRx);
            }
          }
          else
          {
            //logFile.LogData(logName + " error adress: ", ref dataRx);
          }
        }
        else
        {
          //logFile.LogData(logName + " error CRC: ", ref dataRx);
        }
      }

      if(waitReply > 8) //jeden cyklus 50ms
      {
        //v limitu neprisla odpoved, znova se posle req
        isActMaster = false;
        waitReply = 10;
      }
    }

    ////zkopiruje si data k odeslani a pri nejblizsi mozne sanci je odesle
    //public void SendData(ref ByteData _data)
    //{
    //  queueTx.AddData(ref _data);
    //}

    public bool IsRcvData()
    {
      bool tempRet = false;

      if(queueRx.GetCountData() > 0)
      {
        tempRet = true;
      }
      return tempRet;
    }

    //vykopiruje data do zadane reference
    public void GetRcvData(ref ByteData _data)
    {
      queueRx.GetData(ref _data);
    }

    //vrati priznka aktivniho slave
    public bool IsActiveMaster()
    {
      return isActMaster;
    }

    public TelegramCount GetTelegramCount()
    {
      return tlgCount;
    }

    //metody pro povoleni/zakazani blokovani zpravy kn
    public void BlockREQ(bool _block)
    {
      blockMsg.BlockREQ = _block;
    }

    public void BlockACK(bool _block)
    {
      blockMsg.BlockACK = _block;
    }

    public void BlockACK_DATA(bool _block)
    {
      blockMsg.BlockACK_DATA = _block;
    }

    public void BlockRACK(bool _block)
    {
      blockMsg.BlockRACK = _block;
    }

    public void BlockPASS(bool _block)
    {
      blockMsg.BlockPASS = _block;
    }

    public void BlockRPASS(bool _block)
    {
      blockMsg.BlockRPASS = _block;
    }

    //vrati aktualni stav blokovani
    public BlockKnMsg GetBlockKnMsg()
    {
      return blockMsg;
    }

    //povoli/zakaze log portu
    public void EnableLogPort(bool _enable)
    {
      knSerial.EnableLogSerial(_enable);
    }

    //smaze log
    public void ClearLog()
    {
      knSerial.ClearLog();
    }

    public int GetRxErr()
    {
      return knSerial.GetRxErr();
    }

    public string GetErrorString()
    {
      return knSerial.GetErrorString();
    }

    public void ClearErrorString()
    {
      knSerial.ClearErrorString();
    }
  }
}
