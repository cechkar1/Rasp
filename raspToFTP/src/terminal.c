/**
 * @file     terminal.c
 *
 * @brief    Configure terminal connection (tty) 
 * 
 * @author   PeKa, modified by Karel Cech
 * @version  0p2
 * @date     2015
 *
 * Detailed description ...
 *
 * Copyright (c) 2015 PeKa
 * */

#include <termios.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <ctype.h>

#include "terminal.h"


/** Set terminal (tty) attributes
 *
 * @param[in] speed   ..., B9600, ...
 * @param[in] parity  
 *  - 0                      no parity
 *  - PARENB|PARODD          enable parity and use odd
 *  - PARENB                 enable parity and use even
 *  - PARENB|PARODD|CMSPAR   mark parity
 *  - PARENB|CMSPAR          space parity
 * @param[in] timeout time in 0.1s, i.e 50 = 5s 
 *
 * @note
 * http://stackoverflow.com/questions/6947413/
 * 		how-to-open-read-and-write-from-serial-port-in-c
 * cit [2013-07-15]
 * */
int serial_set_attributes(int terminal_file_descriptor, int speed, short int databits, short int stopbit, short int parity, short int RTS)
{
	struct termios tty;

	memset (&tty, 0, sizeof(tty));

	if (tcgetattr (terminal_file_descriptor, &tty) == 0)
	{
		cfsetospeed (&tty, speed);
		cfsetispeed (&tty, speed);

		/* test */


		// disable IGNBRK for mismatched speed tests; otherwise receive break
		// as \000 chars
		tty.c_iflag &= ~IGNBRK;								// ignore break signal
		tty.c_iflag &= ~(IXON | IXOFF | IXANY | ICRNL); 	// shut off xon/xoff ctrl and mapping CR to NL on input.

		tty.c_oflag = 0;									// no remapping, no delays

		tty.c_cflag &= ~CSIZE;
		tty.c_cflag |= databits;
		tty.c_cflag |= (CLOCAL | CREAD);					// ignore modem controls,
		tty.c_cflag &= ~(PARENB | PARODD);
		tty.c_cflag |= parity;
		tty.c_cflag &= ~CSTOPB;
		//tty.c_cflag |= stopbit;
		if (RTS)
			tty.c_cflag |= CRTSCTS;
		else
			tty.c_cflag &= ~CRTSCTS;

		tty.c_lflag = 0;			// no signaling chars, no echo,

		tty.c_cc[VMIN]  = 0;		// read doesn't block
		tty.c_cc[VTIME] = 5;		// 0.5 seconds read timeout

		if (tcsetattr (terminal_file_descriptor, TCSANOW, &tty) == 0)
		{
			return 0;
		}
	}
	return -1;
}
