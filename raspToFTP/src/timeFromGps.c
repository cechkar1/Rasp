#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <linux/serial.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <termios.h>
#include <time.h>
#include <stdint.h>
#include <libxml/parser.h>
#include <libxml/tree.h>
#include <pthread.h>
#include <signal.h>

#include "terminal.h"
#include "mainHeather.h"

#define GPS_SERIAL "/dev/ttyAMA0"
#define BAUDRATE B9600

//not used
void set_time_from_system(){
    time_t rawtime;
    struct tm * timeinfo;
    time ( &rawtime );
    timeinfo = localtime ( &rawtime );
    pthread_mutex_lock(&mutex_lock);
        sprintf(global_time,"%02d%02d%02d%02d%02d%02d",timeinfo->tm_hour,  timeinfo->tm_min
                   ,timeinfo->tm_sec,timeinfo->tm_mday,timeinfo->tm_mon+1,timeinfo->tm_year%100); 
    pthread_mutex_unlock(&mutex_lock);
}
void set_time_with_wrong_timer(unsigned int * wrong_timer){
    pthread_mutex_lock(&mutex_lock);
        sprintf(global_time,"%06d000000",(*wrong_timer)++);
    pthread_mutex_unlock(&mutex_lock);
}
int set_emerygency_time(unsigned int * wrong_timer){
    char znak[1];
    if (config->PCtime) {
        set_time_from_system();
        write_to_log_file("Time is set from PC.\n");
    }
    else {
        set_time_with_wrong_timer(wrong_timer);
        write_to_log_file("Time is set to the emergency format.\n");
    }
    int input_fd = open (GPS_SERIAL, O_RDWR | O_NOCTTY | O_SYNC);
    serial_set_attributes(input_fd, BAUDRATE, CS8, 1, 0, 0);
    fcntl(input_fd,F_SETFD,O_NONBLOCK);
    int read_int =-1;
    while(read_int<1 && g_signal_flag==0 ){
        if (config->PCtime) set_time_from_system();
        else set_time_with_wrong_timer(wrong_timer);
        while (input_fd <0 && g_signal_flag==0){
                    sleep(1);
                    if (config->PCtime) set_time_from_system();
                    else set_time_with_wrong_timer(wrong_timer);    
                    input_fd = open (GPS_SERIAL, O_RDWR | O_NOCTTY | O_SYNC);
                    if (g_signal_flag!=0)
                        return -1;
                    if  (input_fd>1){
                        serial_set_attributes(input_fd, BAUDRATE, CS8, 1, 0, 0);
                        fcntl(input_fd,F_SETFD,O_NONBLOCK);
                    }
        }
        sleep(1);
        read_int=read(input_fd,znak,1);
    }
    if(g_signal_flag==0)
    	write_to_log_file("Time will be set form GPS\n");
    return input_fd;
}

void * getTimeFromGPS(void* v){
        struct sigaction sigact;
        sigemptyset(&sigact.sa_mask);
        sigact.sa_flags   = SA_SIGINFO;
        sigact.sa_handler = sighandler;
        sigaction(SIGUSR1, &sigact, 0);
        
        unsigned int wrong_timer=0;
	time_t timer;
        time (&timer);
	char head[5]="GPRMC";
	char znak[1];
	int i=0,flag=0;
        int read_int;
        //flag just for debug purposes
	int emergency_flag=0;
	char * time_str=(char*)malloc(13*sizeof(char));
	time_str[12]='\0';
        int input_fd = open (GPS_SERIAL, O_RDWR | O_NOCTTY | O_SYNC);
	serial_set_attributes(input_fd, BAUDRATE, CS8, 1, 0, 0);
        fcntl(input_fd,F_SETFD,O_NONBLOCK);
	while (g_signal_flag==0){
                //waiting for GPS port to open, if it isnt, time from the system is used
                if (input_fd <0){
                    input_fd=set_emerygency_time(&wrong_timer);
                    if (input_fd<0)  break;
                }             
                time(&timer);
                read_int=read(input_fd,znak,1);
		if(read_int<1){
			for(i=0;i<2;i++){
				if(read(input_fd,znak,1) >=1) break;
			}
			if(i>=2){
                        	close(input_fd);
                        	input_fd=set_emerygency_time(&wrong_timer);
                        	if (input_fd<0) break;
                        	continue;
			}
                }
                flag=0;
		if(znak[0]=='$'){
                        //loop for finding $GPRMC message
			for (i=0;i<5;i++){
				read(input_fd,znak,1);
				if(znak[0]!=head[i]){
					flag=1;
					break;
				}
			}
                        
                        if(flag) continue;
                        
                        //when the message was found
                        // first sign is a comma
                        read(input_fd,znak,1);
                        //then there should be time in HHMMSS format
                        for(i=0;i<6;i++)
                                read(input_fd,time_str+i,1);
                        i=0;
                        //check whether the time is ok or not char after next ',' must be A
                        for(i=0;i<7;i++){
                            read(input_fd,znak,1);
                            if (znak[0]==',') break;
                        }
                        read(input_fd,znak,1);
                        if (znak[0]!='A') {
                            if (config->PCtime) set_time_from_system();
                            else set_time_with_wrong_timer(&wrong_timer);
			    if(!emergency_flag){
                                if (config->PCtime) write_to_log_file("Time is set from PC\n");
                                else write_to_log_file("Time is set to emergency format.\n");
			    	emergency_flag=1;
			    }
			    usleep(900000);
                            continue;
                        }
                        //the there are 7 fields separated by ','
			i=0;
                        while(i!=7) {
                                read(input_fd,znak,1);
                                if(znak[0]==',')
                                        i++;
                        }
                        //and after 7th comma, there is date in format DDmmYY
                        for(i=6;i<12;i++)
                                read(input_fd,time_str+i,1);
                        //now we should check CRC16...
                        pthread_mutex_lock(&mutex_lock);
                            strcpy (global_time,time_str);
                        pthread_mutex_unlock(&mutex_lock);
			if(emergency_flag){
				emergency_flag=0;
				write_to_log_file("Time is set from GPS.\n");
			}
                        //if we didn't sleep here, read() would read too fast and it would be jumping to set_time_to_zeros()
                        //usleep(900000);

		}
                //when GPS didn't give time after 2 seconds, set the time from system
                if(check_time(&timer,2)){
                        close(input_fd);
                        input_fd=set_emerygency_time(&wrong_timer);
                        if (input_fd<0) break;
                        continue;
                }
	}
        free(time_str);
        if(input_fd>0)
            close(input_fd);
	return NULL;
}

