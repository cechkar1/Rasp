#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <linux/serial.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <termios.h>
#include <stdint.h>
#include <signal.h>
#include "terminal.h"

#define SERIAL "/dev/ttyAMA0"
#define BAUDRATE B9600

char * getTimeFromGPS{
    	int input_fd = open (SERIAL, O_RDWR | O_NOCTTY | O_SYNC);
    	if (input_fd < 0) {
        	return NULL;
    	}
    	///setting the serial port
    	serial_set_attributes(input_fd, BAUDRATE, CS8, 1, 0, 0);
	int flag2=1;
	char head[5]="GPRMC";
	char znak[1];
	int i=0;
	char * time=(char*)malloc(13*sizeof(char);
	time[12]=0;
	while (1){
		flag2=0;
		read(input_fd,znak,1);
		//printf("%c",znak[0]);
		if(znak[0]=='$'){
			for (i=0;i<5;i++){
				read(input_fd,znak,1);
				if(znak[0]!=head[i]){
					flag2=1;
					break;
				}
			}
			if (!flag2){
				read(input_fd,znak,1);
				for(i=0;i<6;i++){
					read(input_fd,znak,1);
					time[i]=znak[0];
				}
				i=0;
				while(i!=8) {
					read(input_fd,znak,1);
					if(znak[0]==',')
						i++;
				}
				for(i=6;i<12;i++){
					read(input_fd,znak,1);
					time[i]=znak[0];
				}
				return time;
			}
		}
	}
	return 0;
}