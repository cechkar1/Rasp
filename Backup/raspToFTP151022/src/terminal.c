/**
 * @file     terminal.c
 *
 * @brief    Configure terminal connection (tty) 
 * 
 * @author   PeKa
 * @version  0p2
 * @date     2015
 *
 * Detailed description ...
 *
 * Copyright (c) 2015 PeKa
 * */

#include <termios.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <ctype.h>

#include "terminal.h"


/** Set terminal (tty) attributes
 *
 * @param[in] speed   ..., B9600, ...
 * @param[in] parity  
 *  - 0                      no parity
 *  - PARENB|PARODD          enable parity and use odd
 *  - PARENB                 enable parity and use even
 *  - PARENB|PARODD|CMSPAR   mark parity
 *  - PARENB|CMSPAR          space parity
 * @param[in] timeout time in 0.1s, i.e 50 = 5s 
 *
 * @note
 * http://stackoverflow.com/questions/6947413/
 * 		how-to-open-read-and-write-from-serial-port-in-c
 * cit [2013-07-15]
 * */
int serial_set_attributes(int terminal_file_descriptor, int speed, short int databits, short int stopbit, short int parity, short int RTS)
{
	struct termios tty;

	memset (&tty, 0, sizeof(tty));

	if (tcgetattr (terminal_file_descriptor, &tty) == 0)
	{
		cfsetospeed (&tty, speed);
		cfsetispeed (&tty, speed);

		/* test */


		// disable IGNBRK for mismatched speed tests; otherwise receive break
		// as \000 chars
		tty.c_iflag &= ~IGNBRK;								// ignore break signal
		tty.c_iflag &= ~(IXON | IXOFF | IXANY | ICRNL); 	// shut off xon/xoff ctrl and mapping CR to NL on input.

		tty.c_oflag = 0;									// no remapping, no delays

		tty.c_cflag &= ~CSIZE;
		tty.c_cflag |= databits;
		tty.c_cflag |= (CLOCAL | CREAD);					// ignore modem controls,
		tty.c_cflag &= ~(PARENB | PARODD);
		tty.c_cflag |= parity;
		tty.c_cflag &= ~CSTOPB;
		//tty.c_cflag |= stopbit;
		if (RTS)
			tty.c_cflag |= CRTSCTS;
		else
			tty.c_cflag &= ~CRTSCTS;

		tty.c_lflag = 0;			// no signaling chars, no echo,

		tty.c_cc[VMIN]  = 0;		// read doesn't block
		tty.c_cc[VTIME] = 5;		// 0.5 seconds read timeout

		if (tcsetattr (terminal_file_descriptor, TCSANOW, &tty) == 0)
		{
			return 0;
		}
	}
	return -1;
}


int
set_tty_attribs(int fd, int speed, int parity, int timeout)
{
	struct termios tty;
	memset (&tty, 0, sizeof(tty) );
	if (tcgetattr(fd, &tty) != 0) {
		return -1;
	}

	cfsetospeed(&tty, speed);
	cfsetispeed(&tty, speed);

	tty.c_cflag = (tty.c_cflag & ~CSIZE) | CS8;     /* 8-bit chars */
	/* disable IGNBRK for mismatched speed tests;
	 * otherwise receive break as \000 chars */
	tty.c_iflag &= ~IGNBRK;         /* ignore break signal */
	tty.c_lflag = 0;                /* no signaling chars, no echo,
									   no canonical processing */
	tty.c_oflag = 0;                /* no remapping, no delays */
	tty.c_cc[VMIN]  = 0;
	tty.c_cc[VTIME] = timeout;      /* timeout in 0.1 sec */

	tty.c_iflag &= ~(IXON | IXOFF | IXANY |ICRNL); /* shut off xon/xoff ctrl */

	tty.c_cflag |= (CLOCAL | CREAD);    /* ignore modem controls,
										   enable reading */
	tty.c_cflag &= ~(PARENB | PARODD);  /*shut off parity */
	tty.c_cflag |= parity;
	tty.c_cflag &= ~CSTOPB; // one stopbit
	tty.c_cflag &= ~CRTSCTS;

	tty.c_cc[VINTR]    = 0x03;
	tty.c_cc[VQUIT]    = 0x1c;
	tty.c_cc[VERASE]   = 0x7f;
	tty.c_cc[VKILL]    = 0x15;
	tty.c_cc[VEOF]     = 0x04;
	tty.c_cc[VTIME]    = 0x05;  /* if 0, `read()` doesn't block (in non-canonical mode) */
	tty.c_cc[VMIN]     = 0x01;  /* if 0, `read()` doesn't block (in non-canonical mode) */
	tty.c_cc[VSWTC]    = 0x00;
	tty.c_cc[VSTART]   = 0x11;
	tty.c_cc[VSTOP]    = 0x13;
	tty.c_cc[VSUSP]    = 0x1a;
	tty.c_cc[VEOL]     = 0x00;
	tty.c_cc[VREPRINT] = 0x12;
	tty.c_cc[VDISCARD] = 0x0f;
	tty.c_cc[VWERASE]  = 0x17;
	tty.c_cc[VLNEXT]   = 0x16;
	tty.c_cc[VEOL2]    = 0x00;

	if (tcsetattr(fd, TCSANOW, &tty) != 0) {
		return -1;
	}
	return 0;
}



/* for VTIME and VMIN explanation see:
 * http://www.unixwiz.net/techtips/termios-vmin-vtime.html
 * */


int
set_tty_GSGL1(int fd)
{
	struct termios tty;
	int rv;

	memset (&tty, 0, sizeof(tty) );
#if 0
	rv = tcgetattr(fd, &tty);
	if (rv) { return -1; }
#endif

	rv = cfsetospeed(&tty, B9600);
	if (rv) { return -1; }
	rv = cfsetispeed(&tty, B9600);
	if (rv) { return -1; }

	tty.c_iflag  = 0;
	tty.c_iflag |= IGNBRK;  /* Ignore BREAK condition on input. */

	tty.c_oflag  = 0;

	/* don't clear c_cflag otherwise the speed set with `cfsetospeed()` and
	 * `cfsetispeed()` will be also cleared */
	//tty.c_cflag  = 0;
	tty.c_cflag  = (tty.c_cflag & ~CSIZE) | CS8;     /* 8-bit chars */
	tty.c_cflag &= ~CSTOPB;   /* use 1 stop bit instead 2 stop bits*/
	tty.c_cflag |=  CREAD;    /* Enable receiver */
	tty.c_cflag &= ~PARENB;   /* disable parity */
	tty.c_cflag &= ~PARODD;   /* If set, then parity odd; otherwise even is used. */
	tty.c_cflag &= ~HUPCL;
	tty.c_cflag |=  CLOCAL;   /* Ignore modem control lines. */
	tty.c_cflag &= ~CMSPAR;   /* Use "stick" (mark/space) parity */
	tty.c_cflag |=  CRTSCTS;  /* Enable  RTS/CTS  (hardware)  flow  control. */

	tty.c_lflag  = 0;
	tty.c_lflag |= NOFLSH;    /* ICANON Enable canonical mode */

	tty.c_line  = 0;

	/* this settings were obtained by investigation of minicom setting */
	tty.c_cc[VINTR]    = 0x03;
	tty.c_cc[VQUIT]    = 0x1c;
	tty.c_cc[VERASE]   = 0x7f;
	tty.c_cc[VKILL]    = 0x15;
	tty.c_cc[VEOF]     = 0x04;
	tty.c_cc[VTIME]    = 0x05;  /* if 0, `read()` doesn't block (in non-canonical mode) */
	tty.c_cc[VMIN]     = 0x01;  /* if 0, `read()` doesn't block (in non-canonical mode) */
	tty.c_cc[VSWTC]    = 0x00;
	tty.c_cc[VSTART]   = 0x11;
	tty.c_cc[VSTOP]    = 0x13;
	tty.c_cc[VSUSP]    = 0x1a;
	tty.c_cc[VEOL]     = 0x00;
	tty.c_cc[VREPRINT] = 0x12;
	tty.c_cc[VDISCARD] = 0x0f;
	tty.c_cc[VWERASE]  = 0x17;
	tty.c_cc[VLNEXT]   = 0x16;
	tty.c_cc[VEOL2]    = 0x00;

	rv = tcsetattr(fd, TCSANOW, &tty);
	if (rv) { return -1; }

	return 0;
} /* end of FUN */



#define show_ifset(str, rflag, BIT) \
	if ( (rflag) & (BIT) ) { fprintf( (str), " --> " #BIT "\n"); }

#define show_subset(str, rflag, SUB, BIT) \
	if ( ( (rflag)&(SUB) ) == (BIT) ) { fprintf( (str), " --> " #SUB ":" #BIT "\n"); }

#define show_c_cc_char(str, c_cc, IDX) \
	fprintf( (str), "c_cc[" #IDX "] = 0x%02x\n", c_cc[(IDX)]); 

/** Show terminal (tty) settings.
 *
 * Print to the stream `str` settings of terminal (tty) identified with file
 * descriptor `fd`.  The switch `details` causes more detailed info is pronted
 * if this switch is set (nonzero).
 *
 * @param[in] `str`        stream, where the information will be printed
 * @param[in] `fd`         file descriptor of terminal (tty)
 * @param[in] `detaisl`    0       -- compact information is provided
 *                         nonzero -- more descriptive info is provided
 * @return    0   -- success
 *           -1   -- problem to obtain info about the terminal
 *
 * */
int
show_tty_attribs(FILE *str, int fd, int details)
{
	struct termios tty;
	memset (&tty, 0, sizeof(tty));
	if (tcgetattr(fd, &tty) != 0) {
		return -1;
	}
	fprintf(str, "c_iflag: 0x%08x (0%011o)\n", (unsigned int)tty.c_iflag, (unsigned int)tty.c_iflag);
	if (details) {
		show_ifset(str, tty.c_iflag, IGNBRK  );
		show_ifset(str, tty.c_iflag, BRKINT  );
		show_ifset(str, tty.c_iflag, IGNPAR  );
		show_ifset(str, tty.c_iflag, PARMRK  );
		show_ifset(str, tty.c_iflag, INPCK   );
		show_ifset(str, tty.c_iflag, ISTRIP  );
		show_ifset(str, tty.c_iflag, INLCR   );
		show_ifset(str, tty.c_iflag, IGNCR   );
		show_ifset(str, tty.c_iflag, ICRNL   );
		show_ifset(str, tty.c_iflag, IUCLC   );
		show_ifset(str, tty.c_iflag, IXON    );
		show_ifset(str, tty.c_iflag, IXANY   );
		show_ifset(str, tty.c_iflag, IXOFF   );
		show_ifset(str, tty.c_iflag, IMAXBEL );
		show_ifset(str, tty.c_iflag, IUTF8   );
	}

	fprintf(str, "c_oflag: 0x%08x (0%011o)\n", (unsigned int)tty.c_oflag, (unsigned int)tty.c_oflag);
	if (details) {
		show_ifset(str, tty.c_oflag, OPOST   );
		show_ifset(str, tty.c_oflag, OLCUC   );
		show_ifset(str, tty.c_oflag, ONLCR   );
		show_ifset(str, tty.c_oflag, OCRNL   );
		show_ifset(str, tty.c_oflag, ONOCR   );
		show_ifset(str, tty.c_oflag, ONLRET  );
		show_ifset(str, tty.c_oflag, OFILL   );
#if defined _BSD_SOURCE || defined _SVID_SOURCE || defined _XOPEN_SOURCE
		show_subset(str, tty.c_oflag, NLDLY,  NL0  );
		show_subset(str, tty.c_oflag, NLDLY,  NL1  );
		show_subset(str, tty.c_oflag, CRDLY,  CR0  );
		show_subset(str, tty.c_oflag, CRDLY,  CR1  );
		show_subset(str, tty.c_oflag, CRDLY,  CR2  );
		show_subset(str, tty.c_oflag, CRDLY,  CR3  );
		show_subset(str, tty.c_oflag, TABDLY,  TAB0 );
		show_subset(str, tty.c_oflag, TABDLY,  TAB1 );
		show_subset(str, tty.c_oflag, TABDLY,  TAB2 );
		show_subset(str, tty.c_oflag, TABDLY,  TAB3 );
		show_subset(str, tty.c_oflag, TABDLY,  XTABS  );
		show_subset(str, tty.c_oflag, BSDLY,  BS0  );
		show_subset(str, tty.c_oflag, BSDLY,  BS1  );
		show_subset(str, tty.c_oflag, FFDLY,  FF0  );
		show_subset(str, tty.c_oflag, FFDLY,  FF1  );
#endif
		show_subset(str, tty.c_oflag, VTDLY,  VT0   );
		show_subset(str, tty.c_oflag, VTDLY,  VT1   );
	}	

	fprintf(str, "c_cflag: 0x%08x (0%011o)\n", (unsigned int)tty.c_cflag, (unsigned int)tty.c_cflag);
	if (details) {
#if defined _BSD_SOURCE || defined _SVID_SOURCE
		show_subset(str, tty.c_cflag,  CBAUD, B0 );
		show_subset(str, tty.c_cflag,  CBAUD, B50 );
		show_subset(str, tty.c_cflag,  CBAUD, B75 );
		show_subset(str, tty.c_cflag,  CBAUD, B110 );
		show_subset(str, tty.c_cflag,  CBAUD, B134 );
		show_subset(str, tty.c_cflag,  CBAUD, B150 );
		show_subset(str, tty.c_cflag,  CBAUD, B200 );
		show_subset(str, tty.c_cflag,  CBAUD, B300 );
		show_subset(str, tty.c_cflag,  CBAUD, B600 );
		show_subset(str, tty.c_cflag,  CBAUD, B1200 );
		show_subset(str, tty.c_cflag,  CBAUD, B1800 );
		show_subset(str, tty.c_cflag,  CBAUD, B2400 );
		show_subset(str, tty.c_cflag,  CBAUD, B4800 );
		show_subset(str, tty.c_cflag,  CBAUD, B9600 );
		show_subset(str, tty.c_cflag,  CBAUD, B19200 );
		show_subset(str, tty.c_cflag,  CBAUD, B38400 );
		show_subset(str, tty.c_cflag,  CBAUD | CBAUDEX, B57600 );
		show_subset(str, tty.c_cflag,  CBAUD | CBAUDEX, B115200 );
		show_subset(str, tty.c_cflag,  CBAUD | CBAUDEX, B230400 );
		show_subset(str, tty.c_cflag,  CBAUD | CBAUDEX, B460800 );
		show_subset(str, tty.c_cflag,  CBAUD | CBAUDEX, B500000 );
		show_subset(str, tty.c_cflag,  CBAUD | CBAUDEX, B576000 );
		show_subset(str, tty.c_cflag,  CBAUD | CBAUDEX, B921600 );
		show_subset(str, tty.c_cflag,  CBAUD | CBAUDEX, B1000000 );
		show_subset(str, tty.c_cflag,  CBAUD | CBAUDEX, B1152000 );
		show_subset(str, tty.c_cflag,  CBAUD | CBAUDEX, B1500000 );
		show_subset(str, tty.c_cflag,  CBAUD | CBAUDEX, B2000000 );
		show_subset(str, tty.c_cflag,  CBAUD | CBAUDEX, B2500000 );
		show_subset(str, tty.c_cflag,  CBAUD | CBAUDEX, B3000000 );
		show_subset(str, tty.c_cflag,  CBAUD | CBAUDEX, B3500000 );
		show_subset(str, tty.c_cflag,  CBAUD | CBAUDEX, B4000000 );
#endif
		show_subset(str, tty.c_cflag,  CSIZE, CS5 );
		show_subset(str, tty.c_cflag,  CSIZE, CS6 );
		show_subset(str, tty.c_cflag,  CSIZE, CS7 );
		show_subset(str, tty.c_cflag,  CSIZE, CS8 );
		show_ifset(str, tty.c_cflag, CSTOPB  );
		show_ifset(str, tty.c_cflag, CREAD   );
		show_ifset(str, tty.c_cflag, PARENB  );
		show_ifset(str, tty.c_cflag, PARODD  );
		show_ifset(str, tty.c_cflag, HUPCL   );
		show_ifset(str, tty.c_cflag, CLOCAL  );
#if defined _BSD_SOURCE || defined _SVID_SOURCE
		show_ifset(str, tty.c_cflag, CMSPAR  );
		show_ifset(str, tty.c_cflag, CRTSCTS );
#endif
	}

	fprintf(str, "c_lflag: 0x%08x (0%011o)\n", (unsigned int)tty.c_lflag, (unsigned int)tty.c_lflag);
	if (details) {
		show_ifset(str, tty.c_lflag, ISIG );
		show_ifset(str, tty.c_lflag, ICANON );
#if defined _BSD_SOURCE || defined _SVID_SOURCE || defined _XOPEN_SOURCE
		show_ifset(str, tty.c_lflag, XCASE );
#endif
		show_ifset(str, tty.c_lflag, ECHO  );
		show_ifset(str, tty.c_lflag, ECHOE );
		show_ifset(str, tty.c_lflag, ECHOK );
		show_ifset(str, tty.c_lflag, ECHONL );
#if defined _BSD_SOURCE || defined _SVID_SOURCE
		show_ifset(str, tty.c_lflag, ECHOCTL );
		show_ifset(str, tty.c_lflag, ECHOPRT );
		show_ifset(str, tty.c_lflag, ECHOKE );
		show_ifset(str, tty.c_lflag, FLUSHO );
#endif
		show_ifset(str, tty.c_lflag, NOFLSH );
		show_ifset(str, tty.c_lflag, TOSTOP );
#if defined _BSD_SOURCE || defined _SVID_SOURCE
		show_ifset(str, tty.c_lflag, PENDIN );
#endif
		show_ifset(str, tty.c_lflag, IEXTEN );
	}

	unsigned char ch;
	ch = (unsigned char)tty.c_line;
	if ( isprint( (int)ch) ) {
		fprintf(str, "c_line:  0x%02x-(%c)\n", ch, ch); 
	} else {
		fprintf(str, "c_line:  0x%02x-(.)\n", ch ); 
	}

	if (details) {
		show_c_cc_char(str, tty.c_cc, VINTR);
		show_c_cc_char(str, tty.c_cc, VQUIT );
		show_c_cc_char(str, tty.c_cc, VERASE );
		show_c_cc_char(str, tty.c_cc, VKILL );
		show_c_cc_char(str, tty.c_cc, VEOF );
		show_c_cc_char(str, tty.c_cc, VTIME );
		show_c_cc_char(str, tty.c_cc, VMIN );
		show_c_cc_char(str, tty.c_cc, VSWTC );
		show_c_cc_char(str, tty.c_cc, VSTART );
		show_c_cc_char(str, tty.c_cc, VSTOP );
		show_c_cc_char(str, tty.c_cc, VSUSP );
		show_c_cc_char(str, tty.c_cc, VEOL );
		show_c_cc_char(str, tty.c_cc, VREPRINT );
		show_c_cc_char(str, tty.c_cc, VDISCARD );
		show_c_cc_char(str, tty.c_cc, VWERASE );
		show_c_cc_char(str, tty.c_cc, VLNEXT );
		show_c_cc_char(str, tty.c_cc, VEOL2 );
	} else {
		unsigned i;
		for (i = 0; i < NCCS; i++) {
			ch = (unsigned char)tty.c_cc[i];
			if ( isprint( (int)ch )) {
				fprintf(str, "[%d]-0x%02x-(%c)", i, ch, ch); 
			} else {
				fprintf(str, "[%d]-0x%02x-(.)", i, ch); 
			}
			if (i < (NCCS-1)) { 
				fprintf(str, ", "); 
			} else {
				fprintf(str, "\n"); 
			}
		}
	}

	fprintf(str, "c_ispeed: 0x%08x\n", (unsigned int)tty.c_ispeed);
	if (details) {
		show_subset(str, tty.c_ispeed,  CBAUD, B0 );
		show_subset(str, tty.c_ispeed,  CBAUD, B50 );
		show_subset(str, tty.c_ispeed,  CBAUD, B75 );
		show_subset(str, tty.c_ispeed,  CBAUD, B110 );
		show_subset(str, tty.c_ispeed,  CBAUD, B134 );
		show_subset(str, tty.c_ispeed,  CBAUD, B150 );
		show_subset(str, tty.c_ispeed,  CBAUD, B200 );
		show_subset(str, tty.c_ispeed,  CBAUD, B300 );
		show_subset(str, tty.c_ispeed,  CBAUD, B600 );
		show_subset(str, tty.c_ispeed,  CBAUD, B1200 );
		show_subset(str, tty.c_ispeed,  CBAUD, B1800 );
		show_subset(str, tty.c_ispeed,  CBAUD, B2400 );
		show_subset(str, tty.c_ispeed,  CBAUD, B4800 );
		show_subset(str, tty.c_ispeed,  CBAUD, B9600 );
		show_subset(str, tty.c_ispeed,  CBAUD, B19200 );
		show_subset(str, tty.c_ispeed,  CBAUD, B38400 );
		show_subset(str, tty.c_ispeed,  CBAUD | CBAUDEX, B57600 );
		show_subset(str, tty.c_ispeed,  CBAUD | CBAUDEX, B115200 );
		show_subset(str, tty.c_ispeed,  CBAUD | CBAUDEX, B230400 );
		show_subset(str, tty.c_ispeed,  CBAUD | CBAUDEX, B460800 );
		show_subset(str, tty.c_ispeed,  CBAUD | CBAUDEX, B500000 );
		show_subset(str, tty.c_ispeed,  CBAUD | CBAUDEX, B576000 );
		show_subset(str, tty.c_ispeed,  CBAUD | CBAUDEX, B921600 );
		show_subset(str, tty.c_ispeed,  CBAUD | CBAUDEX, B1000000 );
		show_subset(str, tty.c_ispeed,  CBAUD | CBAUDEX, B1152000 );
		show_subset(str, tty.c_ispeed,  CBAUD | CBAUDEX, B1500000 );
		show_subset(str, tty.c_ispeed,  CBAUD | CBAUDEX, B2000000 );
		show_subset(str, tty.c_ispeed,  CBAUD | CBAUDEX, B2500000 );
		show_subset(str, tty.c_ispeed,  CBAUD | CBAUDEX, B3000000 );
		show_subset(str, tty.c_ispeed,  CBAUD | CBAUDEX, B3500000 );
		show_subset(str, tty.c_ispeed,  CBAUD | CBAUDEX, B4000000 );
	}

	fprintf(str, "c_ospeed: 0x%08x\n", (unsigned int)tty.c_ospeed);
	if (details) {
		show_subset(str, tty.c_ospeed,  CBAUD, B0 );
		show_subset(str, tty.c_ospeed,  CBAUD, B50 );
		show_subset(str, tty.c_ospeed,  CBAUD, B75 );
		show_subset(str, tty.c_ospeed,  CBAUD, B110 );
		show_subset(str, tty.c_ospeed,  CBAUD, B134 );
		show_subset(str, tty.c_ospeed,  CBAUD, B150 );
		show_subset(str, tty.c_ospeed,  CBAUD, B200 );
		show_subset(str, tty.c_ospeed,  CBAUD, B300 );
		show_subset(str, tty.c_ospeed,  CBAUD, B600 );
		show_subset(str, tty.c_ospeed,  CBAUD, B1200 );
		show_subset(str, tty.c_ospeed,  CBAUD, B1800 );
		show_subset(str, tty.c_ospeed,  CBAUD, B2400 );
		show_subset(str, tty.c_ospeed,  CBAUD, B4800 );
		show_subset(str, tty.c_ospeed,  CBAUD, B9600 );
		show_subset(str, tty.c_ospeed,  CBAUD, B19200 );
		show_subset(str, tty.c_ospeed,  CBAUD, B38400 );
		show_subset(str, tty.c_ospeed,  CBAUD | CBAUDEX, B57600 );
		show_subset(str, tty.c_ospeed,  CBAUD | CBAUDEX, B115200 );
		show_subset(str, tty.c_ospeed,  CBAUD | CBAUDEX, B230400 );
		show_subset(str, tty.c_ospeed,  CBAUD | CBAUDEX, B460800 );
		show_subset(str, tty.c_ospeed,  CBAUD | CBAUDEX, B500000 );
		show_subset(str, tty.c_ospeed,  CBAUD | CBAUDEX, B576000 );
		show_subset(str, tty.c_ospeed,  CBAUD | CBAUDEX, B921600 );
		show_subset(str, tty.c_ospeed,  CBAUD | CBAUDEX, B1000000 );
		show_subset(str, tty.c_ospeed,  CBAUD | CBAUDEX, B1152000 );
		show_subset(str, tty.c_ospeed,  CBAUD | CBAUDEX, B1500000 );
		show_subset(str, tty.c_ospeed,  CBAUD | CBAUDEX, B2000000 );
		show_subset(str, tty.c_ospeed,  CBAUD | CBAUDEX, B2500000 );
		show_subset(str, tty.c_ospeed,  CBAUD | CBAUDEX, B3000000 );
		show_subset(str, tty.c_ospeed,  CBAUD | CBAUDEX, B3500000 );
		show_subset(str, tty.c_ospeed,  CBAUD | CBAUDEX, B4000000 );
	}
        fprintf(str,"kontrola");
	return 0;
}

/* vim:ts=4:cindent:sw=4:foldmethod=syntax:foldcolumn=1
 * */
