#include "mainHeather.h"
#include "terminal.h"

void sighandler(int signum){
    g_signal_flag=1;
}

void check_timers(time_t* log_timer,time_t* upload_timer){
  
    if (check_time(upload_timer,config->up_time*60)){
        upload_file(g_current_output_name,DETACHED);
        time(upload_timer);
        //create new output name
	free(g_current_output_name);
        g_current_output_name=create_output_name(config->logName);
    }
    //one log file pre day
    if (check_time(log_timer,60*60*24*LOG_DURATION)){
        //create new name for log file
        free(g_err_log);
        g_err_log=create_output_name(LOG_END);
        //reset the log timer
        time(log_timer);
        //remove files older than one week
        char log[200];
        sprintf(log ,"find %s -maxdepth 1 -mtime +7 -exec rm -f {} \\;",config->logFold);
        system(log);
    }
}

void read_from_input(int input_fd){
    //initializing the buffer
    unsigned char *buff=(unsigned char*)malloc(MAX_LOG_LENGTH*sizeof(unsigned char));
    memset(buff,0,MAX_LOG_LENGTH*sizeof(unsigned char));
    int bytes_read=0;
        
    int tele_type=0;
    int check=0;
    int zprav=0;
    //we can use a system time, because we are only interested about the difference between two times
    time_t upload_timer;
    time_t log_timer;
    time(&upload_timer);
    time(&log_timer);
    char log[200];
    
    
    while (!g_signal_flag){
        printf("*** Prijato telegramu: %d | zapsano zprav: %d | cas: %c%c-%c%c-%c%c %c%c:%c%c:%c%c ***\r", check,zprav,global_time[10],global_time[11],global_time[8],global_time[9],global_time[6],global_time[7],global_time[0],global_time[1],global_time[2],global_time[3],global_time[4],global_time[5]);
	//Need to do this to refresh the command line
	fflush(stdout);
        /// Check the incoming bytes and if a heather is recognized return which type of telegram it is (and do it untill the signal flag is set to 1)
        tele_type=header_check(input_fd,buff,&bytes_read); // recognizes which type of telegram is on the line
        
        if(tele_type==REQ_ID) 
            ///Handle the ACK telegram
            REQ(input_fd,buff,&bytes_read);
        else if (tele_type==PASS_ID){
            sprintf(log,"Telegram type PASS was recognized after %d telegrams.\n",check);
            write_to_log_file(log);
            //just to know, how many messages were received
            zprav++;
            ///Handle the PASS telegram
            PASS(input_fd,buff,&bytes_read);
        }
        ///When program is interrupted in th middle of reading, don't do anything and end
        else if (tele_type==-1) break;
        ///When the telegram type is unrecognized
        else {
            sprintf(log,"Unrecognized command byte - 0x%02x\n",tele_type & 0xFF);
            write_to_log_file(log);
            clear_buff(buff,&bytes_read,COMMAND_BYTE);
        }
        
        check_timers(&log_timer,&upload_timer);
        ///Just to know how many telegrams were received
        check++;
    }
    
    
    free(buff);
    return;
}


void * thread_main_func(void* nothing){
    ///setting the signalhandler
    struct sigaction sigact;
    sigemptyset(&sigact.sa_mask);
    sigact.sa_flags   = SA_SIGINFO;
    sigact.sa_handler = sighandler;
    sigaction(SIGUSR1, &sigact, 0);
    char log[200];
    ///create log direcotry
    struct stat st = {0};
    if (stat(config->logFold, &st) == -1) mkdir(config->logFold, 0700);
    ///A try to open the port 
    int input_fd = open (config->port, O_RDWR | O_NOCTTY | O_SYNC);
    ///Try to open the port, unitl interrupted
    while (input_fd < 0) {
        sprintf(log,"Couldn't open the port (%s).\r",config->port);
        write_to_log_file(log);
        sleep(1);
        input_fd = open (config->port, O_RDWR | O_NOCTTY | O_SYNC);
        if (g_signal_flag){
            if (input_fd >0) close(input_fd);
            return NULL;
        }
    }
    ///setting the serial port
    serial_set_attributes(input_fd, config->baudrate, CS8, 1, 0, 0);
    
    ///start reading from serial port
    read_from_input(input_fd);
    
    return NULL;
}
