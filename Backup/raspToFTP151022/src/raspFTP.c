/* 
 * File:   raspFTP.c.c
 * Author: karel
 *
 * Created on September 2, 2015, 5:21 PM
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <linux/serial.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <termios.h>
#include <stdint.h>
#include <libxml/parser.h>
#include <libxml/tree.h>

#include "terminal.h"
#include "mainHeather.h"

#define CONFIG_FILE "raspConfig.xml"
/*
 * 
 */

T_config * config=NULL;

int main(int argc, char** argv) {
    fill_config(CONFIG_FILE);
    int input_fd = open (config->port, O_RDWR | O_NOCTTY | O_SYNC);
    if (input_fd < 0) {
        printf("Couldn't open the port (%s).\n",config->port);
        return errno;
    }
    
    serial_set_attributes(input_fd, config->baudrate, CS8, 1, 0, 0);
    char *buff=(char*)malloc(MAX_LOG_LENGTH*sizeof(char));
    memset(buff,0,sizeof(buff));
    
    int bytes_read=0;
    int tele_type=0;
    int check=0;
    int output_fd=-1;
    int output_size=0;
    
    while (1){
        tele_type=header_check(input_fd,buff,&bytes_read); // recognizes which type of telegram is on the line
        if(tele_type==0x0A) REQ(input_fd,buff,&bytes_read);
        else if (tele_type==0x0B){
            printf("pruchod %d\n",check);
            PASS(input_fd,buff,&bytes_read,&output_fd,&output_size);
        }
        else {
            printf("Command number - 0x%02x\n",tele_type & 0xFF);
            memmove(buff,buff+7,(bytes_read-7)*sizeof(*buff)); //remove unrecognized heather from the buffer
            bytes_read-=7;
        }
        check++;
    }
    free(buff);
    free_config();
    close(output_fd);
    close(input_fd);  
    
    
//    char command[MAX_COMMAND_LENGTH]={0};
//    
//    strcat(command,"curl -T ");
//    strcat(command,config->current_fileName);
//    strcat(command," ");
//    strcat(command,config->FTP_IP);
//    strcat(command," -u ");
//    strcat(command,USR_PWD);
//
//    system(command);
//    return (EXIT_SUCCESS);
}

