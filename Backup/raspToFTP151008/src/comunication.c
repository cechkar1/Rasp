
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <linux/serial.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <termios.h>
#include "terminal.h"
#include "mainHeather.h"

int header_check(int fd,char* buff,int *bytes_read){
    int i=0;
    int flag=0;
    char header[4]={0xff,0xff,0xc4,0xd7};
    while(1){
        i=0;
        flag=0;
        while(*bytes_read<7){
            *bytes_read+= read (fd,buff+*bytes_read,MAX_LOG_LENGTH-*bytes_read);
            printf("not enough bytes for heather\n");
        }
        for(i;i<4;i++){
            if (buff[i]==header[i]) {
                continue;
            }
            else {
                memmove(buff,buff+i+1,(*bytes_read-i)*sizeof(*buff));
                *bytes_read -= i+sizeof(*buff);
                printf("Invalid header\n");
                flag=1;
                break;
            }
        }
        if(!flag){
            return buff[6];
        }
    }
}

void REQ(int fd,char reciever,char *buff,int* bytes_read){
    char answer[11]={0xff,0xff,0xc4,0xd7,0x00,0x01,0x8a,0x00,0x00,0x5a,0xac};
    answer[5]=reciever;
    while (*bytes_read < REQ_LEN)
        *bytes_read+=read(fd,buff+*bytes_read,MAX_LOG_LENGTH-*bytes_read);
    write (fd,answer,11);
    //printf("odpoved v poradku\n");
    memmove(buff,buff+REQ_LEN,(*bytes_read-REQ_LEN)*sizeof(*buff));
    *bytes_read-=REQ_LEN;
}
void PASS(int fd,char reciever,char *buff,int* bytes_read){
    char answer[11]={0xff,0xff,0xc4,0xd7,0x00,0x01,0x8b,0x00,0x00,0x5a,0xac};
    answer[5]=reciever;
    while (*bytes_read < PASS_HEATHER_LEN)
        *bytes_read+=read(fd,buff+*bytes_read,MAX_LOG_LENGTH-*bytes_read);
    unsigned int message_size = buff[7];
    message_size = message_size << 8;
    message_size |= buff[8];
    memmove(buff,buff+PASS_HEATHER_LEN,(*bytes_read-PASS_HEATHER_LEN)*sizeof(*buff));
    *bytes_read-=PASS_HEATHER_LEN;
    while (*bytes_read < message_size)
        *bytes_read+=read(fd,buff+*bytes_read,MAX_LOG_LENGTH-*bytes_read);
    int log_fd=open("logF",O_CREAT | O_RDWR,0777);
    write (log_fd,buff,message_size);
    write(fd,answer,11);
    memmove(buff,buff+message_size+2,(*bytes_read-message_size-2)*sizeof(*buff));
    *bytes_read-=message_size+2;
    close(log_fd);
}