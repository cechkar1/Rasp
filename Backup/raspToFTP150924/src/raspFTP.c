/* 
 * File:   main.c
 * Author: karel
 *
 * Created on September 2, 2015, 5:21 PM
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <linux/serial.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <termios.h>
#include "terminal.h"

#define FILE "testFile"
#define FTP_ADD "ftp://srva0.endora.cz"
#define USR_PWD ""
#define MAX_COMMAND_LENGTH 50
#define MAX_LOG_LENGTH 8192
#define PORT "/dev/ttyUSB0"

/*
 * 
 */
int main(int argc, char** argv) {

	int fd = open (PORT, O_RDWR | O_NOCTTY | O_SYNC);
	if (fd < 0) {
            printf("Couldn't open the port (%s).\n",PORT);
            return errno;
	}
	serial_set_attributes(fd, B19200, CS8, 1, 0, 0);
	char send[11]={0};
//	send[0] |= 0b11111111;
//	send[1] |= 0b11111111;
//	send[2] |= 0b11000100;
//	send[3] |= 0b11010111;
//	send[4] |= 0b00000000;
//	send[5] |= 0b00000001;
//	send[6] |= 0b10001011;
//	send[7] |= 0b00000000;
//	send[8] |= 0b00000000;
//	send[9] |= 0b10000110;
//	send[10] |=0b11110110;
//	int sendop = open ("send.txt",O_CREAT | O_RDWR,0666);
//	write(sendop,send,11);
//	close(sendop);
        char message[MAX_LOG_LENGTH];
        strcpy(message,"");
        char output_name[60];
        int i=0;
	int bytes =0;
        for (i;i<6;i++){
            char num[10];
            sprintf(num,"%d",i);
            strcpy(output_name,"log");
            strcat(output_name,num);
            int overall_bytes =0;
            int output = open(output_name,O_CREAT | O_RDWR,0666);
            if (output==-1){
                printf("Couldn't, create the log file\n");
                return errno;
            }
            printf("File %s created.\n",output_name);
            int check;
            while (1){
                usleep(100000);
                memset((void*)message,0,sizeof(message));
		//write(fd,send,11);
                bytes = read (fd,message,MAX_LOG_LENGTH-overall_bytes);
		overall_bytes +=bytes;
                check = write(output,message,bytes);
                if (check!=bytes) printf("Error while writing into the file %s",output_name);
                printf("%s written size = %d message %d\n",message,overall_bytes,bytes);
                if (overall_bytes==MAX_LOG_LENGTH) break;
                else if (overall_bytes > MAX_LOG_LENGTH) printf("WTF?");
            }
            close (output);
        }
    
    
    
//    char command[MAX_COMMAND_LENGTH]={0};
//    
//    strcat(command,"curl -T ");
//    strcat(command,FILE);
//    strcat(command," ");
//    strcat(command,FTP_ADD);
//    strcat(command," -u ");
//    strcat(command,USR_PWD);
//
//    system(command);
//    return (EXIT_SUCCESS);
}

