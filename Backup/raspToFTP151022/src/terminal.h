/**
 * @file     terminal.h
 * @brief    Configure terminal connection (tty)
 * 
 * @author   PeKa
 * @date     2015-06-18
 *
 * */


#ifndef TERMINAL_H__
# define TERMINAL_H__ 1

#include <stdio.h>

int serial_set_attributes(int terminal_file_descriptor, int speed, short int databits, 
                            short int stopbit, short int parity, short int RTS);

extern int
set_tty_attribs(int fd, int speed, int parity, int timeout);

extern int
set_tty_GSGL1(int fd);

extern int
show_tty_attribs(FILE *str, int fd, int details);



#endif /* end of protection macro */


/* vim:ts=4:cindent:sw=4:foldmethod=syntax:foldcolumn=1
 * */
