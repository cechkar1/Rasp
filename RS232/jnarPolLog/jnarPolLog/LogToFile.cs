using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;

namespace jnarPolLog
{
  struct LogFileParams
  {
    public string name;
    public string suffix;
    public bool addDate;
    public bool dateFirst;
    public bool enable;
    public int maxSize;
  }

  class LogToFile
  {
    private TextWriter logFile;
    private string file;  //cely nazev pro zapis
    private LogFileParams logParams;
    private bool open;
    private int writedSize = 0;

    //vytvori novy soubor s aktualnim datem+casem
    private void createNewFile()
    {
      try
      {
        file = "";

        if(logParams.addDate == true)
        {
          if(logParams.dateFirst == false)
          {
            file = logParams.name + "_";
          }
          file += DateTime.Now.Year.ToString().Substring(2);
          file += DateTime.Now.Month.ToString("D2");
          file += DateTime.Now.Day.ToString("D2");
          file += "_";
          file += DateTime.Now.Hour.ToString("D2");
          file += DateTime.Now.Minute.ToString("D2");
          file += DateTime.Now.Second.ToString("D2");

          if(logParams.dateFirst == true)
          {
            file += "_" + logParams.name;
          }
        }
        else
        {
          file = logParams.name;
        }


        file += "." + logParams.suffix;

        if(logParams.enable == true) //vytvorit na disku jen kdyz enable
        {
          logFile = new StreamWriter(file, false);
          logFile.Close();
        }

        open = false;
        writedSize = 0;
      }
      catch(Exception err1)
      {
        MessageBox.Show(err1.Message, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
      }
    }

    //jmeno souboru, vychozi nastaveni povoleni/zakazani zapisu
    //kdyz jmeno souboru "", tak se pouzije jmeno souboru datum+cas -> pokazdy novy log
    //kdyz size > 0, tak po zapisu dane velikosti vytvori novy soubor...
    public LogToFile(LogFileParams _params)
    {
      logParams.name = _params.name;
      logParams.suffix = _params.suffix;
      logParams.addDate = _params.addDate;
      logParams.maxSize = _params.maxSize;
      logParams.enable = _params.enable;
      logParams.dateFirst = _params.dateFirst;

      //po ulozeni vseho mozno vytvorit soubor
      createNewFile();
    }

    public void LogDataYYYYMMDD_HHMMSS(string _text, ref ByteData _data)
    {
      if(logParams.enable == true)
      {
        try
        {
          logFile = new StreamWriter(file, true);
          open = true;

          logFile.Write("{0:d4}-{1:d2}-{2:d2} {3:d2}:{4:d2}:{5:d2}{6}", 
                         DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day,
                         DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second, _text);

          string temp = _data.ToHexStrAll();
          logFile.Write(temp);

          logFile.Write("\n");

          logFile.Close();
          open = false;

          if(logParams.maxSize > 0)  //je pozadavek na deleni
          {
            writedSize += 19 + _text.Length + temp.Length + 1;

            if(writedSize > logParams.maxSize)
            {
              writedSize = 0;
              createNewFile();
            }
          }
        }
        catch
        {
          open = false;
          try
          {
            if(logFile != null)
            {
              logFile.Close();
            }
          }
          catch
          {
          }
        }
      }
    }
    
    //zaloguje radek dat a zavre soubor
    public void LogDataHHMMSS_SSS(string _text, ref ByteData _data)
    {
      if(logParams.enable == true)
      {
        try
        {
          logFile = new StreamWriter(file, true);
          open = true;

          logFile.Write("{0:d2}:{1:d2}:{2:d2},{3:d3}, {4} ", DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second, DateTime.Now.Millisecond, _text);

          string temp = _data.ToHexStrAll();
          logFile.Write(temp);

          logFile.Write("\n");

          logFile.Close();
          open = false;

          if(logParams.maxSize > 0)  //je pozadavek na deleni
          {
            writedSize += 19 + _text.Length + temp.Length + 1;

            if(writedSize > logParams.maxSize)
            {
              writedSize = 0;
              createNewFile();
            }
          }
        }
        catch
        {
          open = false;
          try //jistota...
          {
            if(logFile != null)
            {
              logFile.Close();
            }
          }
          catch
          {
            //nic...
          }
        }
      }
    }

    public void LogText(string _text)
    {
      if(logParams.enable == true)
      {
        try
        {
          logFile = new StreamWriter(file, true);
          open = true;

          logFile.Write("{0:d2}:{1:d2}:{2:d2},{3:d3}, {4} ",
                        DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second, DateTime.Now.Millisecond,
                        _text);
          logFile.Write("\n");

          logFile.Close();
          open = false;

          if(logParams.maxSize > 0)  //je pozadavek na deleni
          {
            writedSize += 19 + _text.Length + 1;

            if(writedSize > logParams.maxSize)
            {
              writedSize = 0;
              createNewFile();
            }
          }

        }
        catch
        {
          open = false;
          try
          {
            if(logFile != null)
            {
              logFile.Close();
            }
          }
          catch
          {
          }
        }
      }
    }

    public bool IsOpen()
    {
      return open;
    }

    public void EnableLog(bool _enable)
    {
      logParams.enable = _enable;
    }

    public void ClearLog()
    {
      bool backupEnable = logParams.enable; //zaloha stavu povoleni
      logParams.enable = false;  //zakaze se logovani

      bool isClear = false;

      do
      {
        if(open == false)
        {
          try
          {
            logFile = new StreamWriter(file, false);
            logFile.Close();
            isClear = true;
          }
          catch
          {
            isClear = false;
          }
        }
      }
      while(isClear == false);


      logParams.enable = backupEnable; //obnovi se stav logovani
    }

  } 

}
