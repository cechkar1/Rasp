﻿using System;


namespace jnarPolLog
{
  struct TelegramCount
  {
    public UInt32 countREQ, countACK, countACK_DATA, countRACK, countPASS, countRPASS;  //pocitadla telegramu

    public TelegramCount(UInt32 countREQ, UInt32 countACK, UInt32 countACK_DATA, 
                         UInt32 countRACK, UInt32 countPASS, UInt32 countRPASS)
    {
      this.countREQ = countREQ;
      this.countACK = countACK;
      this.countACK_DATA = countACK_DATA;
      this.countRACK = countRACK;
      this.countPASS = countPASS;
      this.countRPASS = countRPASS;
    }
  }

  struct BlockKnMsg
  {
    public bool BlockREQ, BlockACK, BlockACK_DATA, BlockRACK, BlockPASS, BlockRPASS;

    public BlockKnMsg(bool BlockREQ, bool BlockACK, bool BlockACK_DATA, 
                      bool BlockRACK, bool BlockPASS, bool BlockRPASS)
    {
      this.BlockREQ      = BlockREQ;
      this.BlockACK      = BlockACK;
      this.BlockACK_DATA = BlockACK_DATA;
      this.BlockRACK     = BlockRACK;
      this.BlockPASS     = BlockPASS;
      this.BlockRPASS    = BlockRPASS;
    }

  }

  struct OcbMsgCount
  {
    public UInt32 rxDataCtrl, rxEmptyCtrl, txDataCtrl, txNoDataCtrl, txNoDataNackCtrl;
    public UInt32 rxDataDiag, rxEmptyDiag, txDataDiag, txNoDataDiag, txNoDataNackDiag;  //pocitadla zprav

    public OcbMsgCount(UInt32 rxDataCtrl, UInt32 rxEmptyCtrl, UInt32 txDataCtrl, UInt32 txNoDataCtrl, UInt32 txNoDataNackCtrl,
                       UInt32 rxDataDiag, UInt32 rxEmptyDiag, UInt32 txDataDiag, UInt32 txNoDataDiag, UInt32 txNoDataNackDiag)
    {
      this.rxDataCtrl       = rxDataCtrl;
      this.rxEmptyCtrl      = rxEmptyCtrl;
      this.txDataCtrl       = txDataCtrl;
      this.txNoDataCtrl     = txNoDataCtrl;
      this.txNoDataNackCtrl = txNoDataNackCtrl;
      this.rxDataDiag       = rxDataDiag;
      this.rxEmptyDiag      = rxEmptyDiag;
      this.txDataDiag       = txDataDiag;
      this.txNoDataDiag     = txNoDataDiag;
      this.txNoDataNackDiag = txNoDataNackDiag;
    }
  }

}
