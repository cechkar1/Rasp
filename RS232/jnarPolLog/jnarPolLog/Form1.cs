﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.IO;
using System.Net;
using System.Threading;


namespace jnarPolLog
{
  struct FtpConfig
  {
    public string ip;
    public string path;
    public string login;
    public string pswd;
    public int    updTime;
    public bool   enabled;
    public string fileName;
  }

  public partial class Form1 : Form
  {
    private KnDriverSl portKn;
    private ByteData dataRx;  //buffer pro predavani a pripadne upravy zprav 
    private LogToFile log;
    private LogToFile logError;
    private SimpleLogToFile simpleLog;
    private string simpleLogName;
    private static string ftpLogName = ""; //musi obsahovat jen jemo souboru - pri uploadu na ftp pak
    private DateTime timeLogEnd;
    private Thread t;
    private static int ftpUplOk = 0;
    private static int ftpUplErr = 0;
    private static FtpConfig ftpCfg;
    private static string statusString = "";  //string pouzivany pro zobrazeni v statusStrip
    private static string ftpErrString = "";  //string pro chyby ftp logovany do err souboru
    

    public Form1()
    {
      InitializeComponent();
    }

    //vraci true pri retezci TRUE/true
    private bool isStringTrue(string _data)
    {
      bool result = false;
      if(_data == "TRUE" || _data == "true")
      {
        result = true;
      }

      return result;
    }

    private void createNewSimpleLog()
    {
      simpleLogName = DateTime.Now.Year.ToString().Substring(2);
      simpleLogName += DateTime.Now.Month.ToString("D2");
      simpleLogName += DateTime.Now.Day.ToString("D2");
      simpleLogName += "_";
      simpleLogName += DateTime.Now.Hour.ToString("D2");
      simpleLogName += DateTime.Now.Minute.ToString("D2");
      simpleLogName += DateTime.Now.Second.ToString("D2");
      simpleLogName += ftpCfg.fileName;
      simpleLog.OpenFile(simpleLogName, false);
    }

    
    private static void errFtp()
    {
      //presun souboru...
      try
      {
        System.IO.File.Move(ftpLogName, @"ftp_err\" + ftpLogName);
      }
      catch
      {
      }

      ftpUplErr++; //inc pocitadla chyb
    }

    private static void uploadFile()
    {
      bool err = false;
      try
      {
        //obslehnuto z http://geekswithblogs.net/VROD/archive/2011/11/11/147657.aspx

        //vytvoreni ftp pozadavku
        string ftpStr = "ftp://" + ftpCfg.ip + ftpCfg.path + ftpLogName;
        //FtpWebRequest ftpWebRequest = (FtpWebRequest)WebRequest.Create("ftp://srva0.endora.cz/include/tables/jnar_data/test.txt");
        FtpWebRequest ftpWebRequest = (FtpWebRequest)WebRequest.Create(ftpStr);
        ftpWebRequest.Method = WebRequestMethods.Ftp.UploadFile;

        //nastaveni hesla a jmena pro ftp
        ftpWebRequest.Credentials = new NetworkCredential(ftpCfg.login, ftpCfg.pswd);

        // set proxy = null! - bez toho to nefunguje na proxy - dabelske
        ftpWebRequest.Proxy = null;

        //nastaveni souboru do streamu
        StreamReader sourceStream = new StreamReader(ftpLogName);
        byte[] fileContents = Encoding.UTF8.GetBytes(sourceStream.ReadToEnd());
        sourceStream.Close();
        ftpWebRequest.ContentLength = fileContents.Length;

        //a stream na ftp pak
        Stream requestStream = ftpWebRequest.GetRequestStream();
        requestStream.Write(fileContents, 0, fileContents.Length);
        requestStream.Close();

        //vytahnuti response
        FtpWebResponse response = (FtpWebResponse)ftpWebRequest.GetResponse();
        statusString = "FTP: " + response.StatusDescription; //uloznei
        if(response.StatusDescription.Length > 2) //pro jistotu kontrola delky response pred rezanim
        {
          statusString = statusString.Substring(0, statusString.Length - 2); //oriznuti dvou poslednich znaku odradkovani...
        }

        //kdyz neni response ok, tak presun souboru...
        if(statusString.Contains("226") == true)
        {
          System.IO.File.Move(ftpLogName, @"ftp_upl\" + ftpLogName);
          ftpUplOk++;
        }
        else
        {
          errFtp();
          err = true;
        }

      }
      catch(Exception e)
      {
        //kdyby retezeni vyjimek....
        if(err == false)
        {
          errFtp();
        }

        statusString = "FTP Err: " + e.Message.ToString();
        ftpErrString = "FTP Err: " + e.Message.ToString();
      }

      
      
      ftpLogName = ""; //priznak, ze je vse nejak dokonano

    }

    private void Form1_Load(object sender, EventArgs e)
    {
      labelData.Text = "0";

      labelFtpOk.Text  = "0";
      labelFtpErr.Text = "0";
      
      dataRx = new ByteData(1500);

      LogFileParams logParams;

      //naceni konfigurace z xml
      XmlDocument conf = new XmlDocument();

      try
      {
        //vytvoreni adresaru pro odkladani souboru po praci s ftp
        System.IO.Directory.CreateDirectory("ftp_upl");
        System.IO.Directory.CreateDirectory("ftp_err");
      }
      catch
      {
      }

      try
      {
        conf.Load("conf.xml");
        XmlNode item = conf.SelectNodes("/JNARPOLOG/PORT")[0];
        string comPort = "COM" + item.SelectSingleNode("COM").InnerText;
        int baudrate = Convert.ToInt32(item.SelectSingleNode("baudrate").InnerText);

        item = conf.SelectNodes("/JNARPOLOG/LOG_COM")[0];
        logParams.enable = isStringTrue(item.SelectSingleNode("logEnable").InnerText);
        logParams.name = comPort +"_log";
        logParams.suffix = item.SelectSingleNode("logSuffix").InnerText;
        logParams.addDate = isStringTrue(item.SelectSingleNode("addDate").InnerText);
        logParams.dateFirst = isStringTrue(item.SelectSingleNode("dateFirst").InnerText);
        logParams.maxSize = Convert.ToInt32(item.SelectSingleNode("maxSize").InnerText);

        portKn = new KnDriverSl(1, comPort, baudrate, logParams);

        item = conf.SelectNodes("/JNARPOLOG/LOG_DATA")[0];
        logParams.enable = isStringTrue(item.SelectSingleNode("logEnable").InnerText);
        logParams.name = item.SelectSingleNode("logName").InnerText;
        logParams.suffix = item.SelectSingleNode("logSuffix").InnerText;
        logParams.addDate = isStringTrue(item.SelectSingleNode("addDate").InnerText);
        logParams.dateFirst = isStringTrue(item.SelectSingleNode("dateFirst").InnerText);
        logParams.maxSize = Convert.ToInt32(item.SelectSingleNode("maxSize").InnerText);

        log = new LogToFile(logParams);

        item = conf.SelectNodes("/JNARPOLOG/LOG_ERR")[0];
        logParams.enable = true; //vzdy log
        logParams.name = item.SelectSingleNode("logName").InnerText;
        logParams.suffix = item.SelectSingleNode("logSuffix").InnerText;
        logParams.addDate = isStringTrue(item.SelectSingleNode("addDate").InnerText);
        logParams.dateFirst = isStringTrue(item.SelectSingleNode("dateFirst").InnerText);
        logParams.maxSize = Convert.ToInt32(item.SelectSingleNode("maxSize").InnerText);

        logError = new LogToFile(logParams);
        
        groupBoxPort.Text = comPort + "  " + Convert.ToString(baudrate) + "kbit/s";


        item = conf.SelectNodes("/JNARPOLOG/FTP_LOG")[0];
        ftpCfg.enabled = isStringTrue(item.SelectSingleNode("logEnable").InnerText);
        if(ftpCfg.enabled == true)  //kdyz je povoleno, tak resit zbytek...
        {
          ftpCfg.ip = item.SelectSingleNode("IP").InnerText;
          ftpCfg.path = item.SelectSingleNode("path").InnerText;
          ftpCfg.updTime = Convert.ToInt32(item.SelectSingleNode("updTime").InnerText);
          ftpCfg.fileName = item.SelectSingleNode("logName").InnerText;
          ftpCfg.fileName += ".";
          ftpCfg.fileName += item.SelectSingleNode("logSuffix").InnerText;
          //TODO taky v citelnej cfg?
          ftpCfg.login = "jnar_azd";
          ftpCfg.pswd = "jnar_azd";
          groupBoxFtp.Text = "ftp://" + ftpCfg.ip + ftpCfg.path;
          if(groupBoxFtp.Text.Length > 50) //oriznuti vypisu pri prekroceni deilky...
          {
            groupBoxFtp.Text = groupBoxFtp.Text.Substring(0, 45) + "...";
          }
        }


        timer1.Enabled = true;
      }
      catch(Exception err1)
      {
        if(err1.Message.Contains("Object reference not set to"))
        {
          MessageBox.Show("Chyba v xml souboru", "ERROR - load conf.xml", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
        else
        {
          MessageBox.Show(err1.Message, "ERROR - load conf.xml", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        this.Close();
      }

      if(ftpCfg.enabled == true)  //ftp jen kdyz je povoleno
      {
        //vytvoreni veci pro log omezeny casem
        timeLogEnd = new DateTime();
        timeLogEnd = DateTime.Now;
        timeLogEnd = timeLogEnd.AddMinutes(ftpCfg.updTime);

        simpleLog = new SimpleLogToFile();
        createNewSimpleLog();
      }
    }

    private void timer1_Tick(object sender, EventArgs e)
    {
      try
      {
        while(portKn.IsRcvData() == true)
        {
          dataRx.Clear();
          portKn.GetRcvData(ref dataRx);
          log.LogDataYYYYMMDD_HHMMSS("; ", ref dataRx);

          if(ftpCfg.enabled == true)  //ftp jen kdyz je povoleno, tak zaznam do simplelogu
          {
            simpleLog.LogDataYYYYMMDD_HHMMSS("; ", ref dataRx);
          }

          //ma cenu aktualizovat jen po prijmu dat
          TelegramCount tlgCount = portKn.GetTelegramCount();

          labelData.Text = Convert.ToString(tlgCount.countPASS);
        }

        if(portKn.IsActiveMaster() == true)
        {
          panelActive.BackColor = System.Drawing.Color.Green;
        }
        else
        {
          panelActive.BackColor = System.Drawing.Color.Red;
        }

        labelKNError.Text = Convert.ToString(portKn.GetRxErr());
        string err = portKn.GetErrorString();
        if(err != "")
        {
          logError.LogText(err);
          portKn.ClearErrorString();
        }

        if(ftpCfg.enabled == true)
        {
          if(DateTime.Now > timeLogEnd) //je ftp povoleno a prisel cas uploadu
          {
            //prisel cas poslani logu na ftp a vytvoreni noveho...
            simpleLog.Close(); //zavreni souboru

            if(ftpLogName == "")  //odminula dokonceno
            {
              statusString = "FTP: začátek uploadu...";

              ftpLogName = simpleLogName; //odzalohovani jmena souboru
              t = new Thread(uploadFile); //vlakno na upload
              t.Start();
            }
            else
            {
              //soubor se nepovedlo poslat na ftp - zkusit shodit vlakno a uklizeni do nejake jine slozky
              statusString = "FTP: chyba uploadu - nezpracovaný požadavek z minula";

              try
              {
                t.Abort();
              }
              catch
              {
              }

              //presun souboru...
              try
              {
                System.IO.File.Move(ftpLogName, @"ftp_err\" + ftpLogName);
              }
              catch
              {
              }

              ftpUplErr++;
              ftpLogName = ""; //smaznuti pro dalsi praci...

            }


            createNewSimpleLog();      //vytvoreni noveho logu + casu
            timeLogEnd = DateTime.Now;
            timeLogEnd = timeLogEnd.AddMinutes(ftpCfg.updTime);

          }
          else
          {
            //neni cas k oeslani - update
            TimeSpan toUpd = timeLogEnd - DateTime.Now;
            labelTimeToUpdateFtp.Text = toUpd.Minutes.ToString("D2") + ":" + toUpd.Seconds.ToString("D2");
          }

          //update ftp zobrazovadel
          labelFtpOk.Text = Convert.ToString(ftpUplOk);
          labelFtpErr.Text = Convert.ToString(ftpUplErr);
        }
        
        //update status
        if(statusString.Length > 50)
        {
          toolStripStatusLabel1.Text = statusString.Substring(0, 50);
        }
        else
        {
          toolStripStatusLabel1.Text = statusString;
        }

        //update err souboru
        if(ftpErrString != "")
        {
          logError.LogText(ftpErrString);
          ftpErrString = "";
        }
      }
      catch(Exception exc)
      {
        statusString = exc.Message.ToString();
      }
     
    }

    private void button1_Click(object sender, EventArgs e)
    {
      ftpLogName = "test.txt";
      uploadFile();
    }

  }
}
