
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <linux/serial.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <termios.h>
#include <time.h>
#include <stdint.h>
#include <libxml/parser.h>
#include <libxml/tree.h>

#include "terminal.h"
#include "mainHeather.h"

extern T_config * config;


void create_output_name(int gps_time){
    if(!GPS_TIME){ //time from the operating system
        time_t rawtime;
        struct tm * timeinfo;

        time ( &rawtime );
        timeinfo = localtime ( &rawtime );
        free(config->current_fileName);
        config->current_fileName=(char*)malloc(50*sizeof(char));
        sprintf(config->current_fileName,"%02d%02d%02d_%02d%02d%02d%s",timeinfo->tm_year-100,timeinfo->tm_mon+1,timeinfo->tm_mday,timeinfo->tm_hour,timeinfo->tm_min,timeinfo->tm_sec,config->logName);
        }
    else return /*time_from_gps*/;
}
char * get_time_for_output(int gps_time){
    if(!GPS_TIME){// time form the operating system
        time_t rawtime;
        struct tm * timeinfo;
        time ( &rawtime );
        timeinfo = localtime ( &rawtime );
        char* log_time=(char*)malloc(50*sizeof(char));
        sprintf(log_time,"%02d-%02d-%02d %02d:%02d:%02d; ",timeinfo->tm_year-100,timeinfo->tm_mon+1,timeinfo->tm_mday,timeinfo->tm_hour,timeinfo->tm_min,timeinfo->tm_sec);
        
        return log_time;
        }
    else return /*time_from_gps*/ "fce jeste neni...";
}
char * get_CRC16(char *input,int n){
	uint16_t pom=0, i=0;
	uint16_t inic = 0xffff;
	for(i; i < n; i++){
            input[i]&=0xFF;
            pom = CRCTab[(uint8_t)((uint16_t)input[i] ^ inic)];
            inic = pom ^ ((inic >> 8) & 0x00FF);
	}
	inic = inic^0xFFFF;
        char * crc = (char*)malloc(2*sizeof(char));
        printf("%02x\n",inic);
        crc[0]= inic; //we have to switch the bytes
        crc[1]= inic >> 8;
        return crc;
}
int check_CRC16(char *message,int size){
    char * crc = get_CRC16(message,size); //two bytes with CRC
    if (*(message + size)==crc[0] && *(message + size+1)==crc[1]) {
        printf("Good CRC16 %02x%02x\n",crc[0]& 0xFF,crc[1]& 0xFF); //we have to & chars with 0xFF everytime because of reasons... without it it sometimes prints "ffffffAB" instead of AB.
        free(crc);
        return 0;
    }
    else {
        printf("Wrong CRC16 %02x%02x correct - %02x%02x\n",crc[0]& 0xFF,crc[1]& 0xFF,*(message + size)& 0xFF,*(message + size+1)& 0xFF);
        free(crc);
        return 1;
    }
} 
int open_new_output_file(int * output_fd,int * output_size){
        create_output_name(GPS_TIME);
        *output_fd=open(config->current_fileName, O_CREAT | O_RDWR,0666);
        *output_size=0;
        if (*output_fd < 0) {
            printf("Couldnt create a new file(%s).\n",config->current_fileName);
            return 1;
        }
        return 0;
}
void write_to_output(char * message, int size,int * output_fd,int * output_size){
    if (*output_fd==-1){
        if(open_new_output_file(output_fd, output_size))
            return;
    }
    else if (*output_size>config->logSize){
        close(*output_fd);
        if(open_new_output_file(output_fd, output_size))
            return;
    }
    
    char* log_time = get_time_for_output(GPS_TIME);
    write(*output_fd,log_time,strlen(log_time)); //adding timestamp into the log
    free(log_time);
    
    int i=0;
    char byte[3]; //3 because of \0
    for(i;i<size;i++){
        printf("%02x ",message[i] & 0xFF);
        sprintf(byte,"%02x",message[i] & 0xFF); //we have to format the bytes, because log is in ASCII format
        write(*output_fd,byte,2);
        if(i!=size-1) write(*output_fd,",0x",3);
        else {
            write(*output_fd,"\n",1);
        }
    }
    
    printf("\n");
    output_size+=3*size+19; //19 is length of a time log... 3*size -> two hexadecimal digits + '-' (/n in the end)
}

int header_check(int fd,char* buff,int *bytes_read){
    int i=0;
    int flag=0;//flag for checking whether the heather war read correctly or not.
    char header[4]={0xff,0xff,0xc4,0xd7};
    while(1){
        i=0;
        flag=0;
        while(*bytes_read<7){ //we need to read 7 bytes, because 7th byte is a type of telegram, which is returned
            *bytes_read+= read (fd,buff+*bytes_read,MAX_LOG_LENGTH-*bytes_read);
            //printf("not enough bytes for heather\n");
        }
        for(i;i<4;i++){
            if (buff[i]==header[i]) {
                continue;
            }
            else {
                memmove(buff,buff+i+1,(*bytes_read-i)*sizeof(*buff)); //when the heather sequence is corrupted, forget it.
                *bytes_read -= i+sizeof(*buff);
                flag=1;
                break;
            }
        }
        if(!flag){
            return buff[6];
        }
    }
}

int read_rest_of_message(int input_fd, char * buff, int * bytes_read){
    while (*bytes_read < HEATHER_LEN) // we need to know the data length first
        *bytes_read+=read(input_fd,buff+*bytes_read,MAX_LOG_LENGTH-*bytes_read);
    unsigned int message_size = buff[7];
    message_size = message_size << 8;
    message_size |= buff[8]; 
    while (*bytes_read < message_size+HEATHER_LEN+2)       //we will read the whole log into the buffer (+2 for CRC)                                                 
        *bytes_read+=read(input_fd,buff+*bytes_read,MAX_LOG_LENGTH-*bytes_read-PASS_HEATHER_LEN);
    printf("bytes read %d, message_size %d\n",*bytes_read,message_size);
    
    if(check_CRC16(buff+4,message_size+5)){        
        memmove(buff,buff + HEATHER_LEN,(*bytes_read-HEATHER_LEN)*sizeof(*buff)); //we have to skip the heather (and the two bytes of length)  only, there may be valid data after it
        *bytes_read-=HEATHER_LEN;
        printf("Wrong CRC16\n");
        return 1;
    }
}


void REQ(int input_fd,char *buff,int* bytes_read){ //this program counts with slave not having any message for master -> we don't have to check CRC16 here
    char answer[11]={0xff,0xff,0xc4,0xd7,0x00,0x01,0x8a,0x00,0x00,0x5a,0xac};
    answer[5]=buff[4]; //putting a slave ID into the answer. ID different from 01 will cause wrong checksum in the answer (probably should fix this).
    while (*bytes_read < REQ_HEATHER_LEN)
        *bytes_read+=read(input_fd,buff+*bytes_read,MAX_LOG_LENGTH-*bytes_read);
    write (input_fd,answer,11);
    
    memmove(buff,buff+REQ_HEATHER_LEN,(*bytes_read-REQ_HEATHER_LEN)*sizeof(*buff));// skipping only heather, because we don't check CRC and there MAY be a new block of data after it
    *bytes_read-=REQ_HEATHER_LEN;
}
void PASS(int input_fd,char *buff,int* bytes_read,int * output_fd,int * output_size){
    char answer[11]={0xff,0xff,0xc4,0xd7,0x00,0x01,0x8b,0x00,0x00,0x86,0xf6};
    answer[5]=buff[4];//putting a slave ID into the answer. ID different from 01 will cause wrong checksum in the answer (probably should fix this).
    
    while (*bytes_read < PASS_HEATHER_LEN) // we need to know the data length first
        *bytes_read+=read(input_fd,buff+*bytes_read,MAX_LOG_LENGTH-*bytes_read);
    unsigned int message_size = buff[7];
    message_size = message_size << 8;
    message_size |= buff[8]; 
    while (*bytes_read < message_size+PASS_HEATHER_LEN+2)       //we will read the whole log into the buffer (+2 for CRC)                                                 
        *bytes_read+=read(input_fd,buff+*bytes_read,MAX_LOG_LENGTH-*bytes_read-PASS_HEATHER_LEN);
    printf("bytes read %d, message_size %d\n",*bytes_read,message_size);
    
    if(check_CRC16(buff+4,message_size+5)){        
        memmove(buff,buff + PASS_HEATHER_LEN,(*bytes_read-PASS_HEATHER_LEN)*sizeof(*buff)); //we have to skip the heather (and the two bytes of length)  only, there may be valid data after it
        *bytes_read-=PASS_HEATHER_LEN;
        printf("Wrong CRC16\n");
        return;
    }
    
    write_to_output(buff,message_size+2+PASS_HEATHER_LEN,output_fd,output_size); 
    printf("Message written\n");
    write(input_fd,answer,11);
    
    memmove(buff,buff+message_size+PASS_HEATHER_LEN+2,(*bytes_read-PASS_HEATHER_LEN-message_size-2)*sizeof(*buff)); // add 2 for CRC16
    *bytes_read-=message_size+PASS_HEATHER_LEN+2;
}
void fill_struct(xmlNode *cur,xmlDoc * doc){
    cur = cur->xmlChildrenNode;
    while (cur != NULL) {
        if ((!strcmp(cur->name, "FTP_IP"))){
            config->FTP_IP = xmlNodeListGetString(doc, cur->xmlChildrenNode, 1);
            printf("%s\n",config->FTP_IP);
        }
        if ((!strcmp(cur->name, "baudrate"))){
            char *tmp = xmlNodeListGetString(doc, cur->xmlChildrenNode, 1);
            int baudrate = atoi(tmp);
            free(tmp); //we have to free the string...
                 if(baudrate==300) config->baudrate = B300; // now we have to match a correct predefined value
            else if(baudrate==1200) config->baudrate = B1200;
            else if(baudrate==4800) config->baudrate = B4800;
            else if(baudrate==9600) config->baudrate = B9600;
            else if(baudrate==19200) config->baudrate = B19200;
            else if(baudrate==38400) config->baudrate = B38400;
            else if(baudrate==57600) config->baudrate = B57600;
            else if(baudrate==115200) config->baudrate = B115200;
            else if(baudrate==230400) config->baudrate = B230400;
            else printf("Wrong baudrate is set. Using default...");
            printf("%d\n",baudrate);
        }
        if ((!strcmp(cur->name, "logName"))){
            config->logName = xmlNodeListGetString(doc, cur->xmlChildrenNode, 1);
            printf("%s\n",config->logName);
        }
         if ((!strcmp(cur->name, "logSize"))){
            char *tmp = xmlNodeListGetString(doc, cur->xmlChildrenNode, 1);
            config->logSize = atoi(tmp);
            free(tmp);
            printf("%d\n",config->logSize);
        }
         if ((!strcmp(cur->name, "port"))){
            config->port = xmlNodeListGetString(doc, cur->xmlChildrenNode, 1);
            printf("%s\n",config->port);
        }
        cur = cur->next;
    }
}
set_default_config(){
        config =(T_config*)malloc(sizeof(T_config));
        config->port="/dev/ttyUSB0";
	config->baudrate=B19200;
	config->FTP_IP="srva0.endora.cz";
	config->logName="log.txt";
	config->logSize=180;
        config->current_fileName=NULL;
}
int fill_config(char * config_file){
   
    set_default_config();
    
    xmlDoc *doc = NULL;
    xmlNode *root_element = NULL;

    /*
     * this initialize the library and check potential ABI mismatches
     * between the version it was compiled for and the actual shared
     * library used.
     */
    LIBXML_TEST_VERSION

    /*parse the file and get the DOM */
    doc = xmlReadFile(config_file, NULL, 0);

    if (doc == NULL) {
        printf("error: could not parse configuration file. Using default...\n");
        return 1;
    }
    /*Get the root element node */
    root_element = xmlDocGetRootElement(doc);

    fill_struct(root_element,doc);

    /*free the document */
    xmlFreeDoc(doc);

    /*
     *Free the global variables that may
     *have been allocated by the parser.
     */
    xmlCleanupParser();

    return 0;
}

void free_config(){
    free(config->FTP_IP);
    free(config->logName);
    free(config->port);
    free(config->current_fileName);
    free(config);
}